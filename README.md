# README #

This repository contains projects for cis2232

### Background ###

This project will provide administrative functions for all cis2232 projects.  It can also act as a template for projects.  It contains examples of all major concepts from cis2232.  This includes dao database access, Spring MVC with Thymeleaf, web services (rest/soap).  The interaction of projects with this project is setup in the Canes Camp project.  This can be used to see how to use the cisadmin for authentication and code type functionality.


### Setup ###
Download the project.  in the other sources locate the db.mysql/codeTypes.sql file.  This will create the database needed to run the project.  Build with dependencies if required and run the project on Tomcat.  


BJ MacLean