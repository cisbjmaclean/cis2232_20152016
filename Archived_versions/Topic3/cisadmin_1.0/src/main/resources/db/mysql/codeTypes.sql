-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2015 at 04:06 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codes`
--

-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS `CodeType` (
  `codeTypeId` int(3) NOT NULL COMMENT 'This is the primary key for code types',
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

--
-- Dumping data for table `code_type`
--

INSERT INTO `codeType` (`codeTypeId`, `englishDescription`, `frenchDescription`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 'User Types', 'User Types FR', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(2, 'NewOne', NULL, NULL, NULL, NULL, NULL),
(3, 'NewAnotherOne', NULL, NULL, NULL, NULL, NULL);


CREATE TABLE IF NOT EXISTS `CodeValue` (
  `codeTypeId` int(3) NOT NULL COMMENT 'see code_type table',
  `codeValueSequence` int(3) NOT NULL,
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `englishDescriptionShort` varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `frenchDescriptionShort` varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';


INSERT INTO `CodeValue` (`codeTypeId`, `codeValueSequence`, `englishDescription`, `englishDescriptionShort`, `frenchDescription`, `frenchDescriptionShort`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 1, 'Administrator', 'Administrator', 'AdminFR', 'AdminFR', '2015-10-25 18:44:23', 'admin', '2015-10-25 18:44:23', 'admin'),
(1, 2, 'General', 'General', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(1, 3, 'View Only', 'View Only', 'testFR', 'testFR', '2015-11-06 09:44:13', 'admin', '2015-11-06 09:44:13', 'admin'),
(1, 4, 'Quality Control', 'QC', NULL, NULL, NULL, NULL, NULL, NULL),
(1, 5, 'Quality Control', 'QC', NULL, NULL, NULL, NULL, NULL, NULL),
(1, 6, 'Quality Control', 'QC', NULL, NULL, NULL, NULL, NULL, NULL),
(1, 7, 'Quality Control', 'QC', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 8, 'a', 'a', NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `code_type`
--
ALTER TABLE `CodeType`
  ADD PRIMARY KEY (`codeTypeId`);

--
-- Indexes for table `code_value`
--
ALTER TABLE `CodeValue`
  ADD PRIMARY KEY (`codeValueSequence`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `code_type`
--
ALTER TABLE `CodeType`
  MODIFY `CodeTypeId` int(3) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key for code types',AUTO_INCREMENT=1;

ALTER TABLE `CodeValue`
  MODIFY `codeValueSequence` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
