package info.hccis.admin.web;

import info.hccis.admin.data.springdatajpa.CodeTypeRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.CodeType;
import info.hccis.admin.model.jpa.CodeValue;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    private final CodeTypeRepository ctr;
    private final CodeValueRepository cvr;

    @Autowired
    public OtherController(CodeTypeRepository ctr, CodeValueRepository cvr){ 
        this.ctr = ctr;
        this.cvr = cvr;
    }

    @RequestMapping("/other/futureUse")
    public String showFutureUse(Model model) {
        return "other/futureUse";
    }

    @RequestMapping("/other/about")
    public String showAbout(Model model) {
        System.out.println("Count from code_type="+ctr.count());
        
        ArrayList<CodeType> cts = (ArrayList<CodeType>) ctr.findByEnglishDescription("User Types");
        System.out.println("BJM-"+cts);
        
        ArrayList<CodeType> codeTypesByBob = (ArrayList<CodeType>) ctr.findByCreatedUserId("Bob");
     
        ArrayList<CodeValue> codeValues = (ArrayList<CodeValue>) cvr.findAll();
        System.out.println("Here are the Code values");     
        for(CodeValue thisOne: codeValues){
            System.out.println(thisOne);
        }
        System.out.println("Here are the Code types created by bob");     
        for(CodeType thisOne: codeTypesByBob){
            System.out.println(thisOne);
        }
        
//        //Create a new code value here.
//        CodeValue newCodeValue = new CodeValue(4,1,"TestNew","TN2");
//        cvr.save(newCodeValue);
        return "other/about";
    }

    @RequestMapping("/other/help")
    public String showHelp(Model model) {
        return "other/help";
    }

    @RequestMapping("/")
    public String showHome(Model model) {
        System.out.println("in controller for /");
        //setup the databaseConnection object in the model.  This will be used on the 
        //view.
        DatabaseConnection databaseConnection = new DatabaseConnection();
        model.addAttribute("databaseConnection", databaseConnection);

        return "other/databaseInformation";
    }

}
