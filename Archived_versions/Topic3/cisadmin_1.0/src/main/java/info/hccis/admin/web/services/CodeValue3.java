package info.hccis.admin.web.services;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

@WebService(serviceName = "CodeValue3")
public class CodeValue3 {

    /**
     * This is a sample web service operation
     * @param operand1
     * @param operand2
     * @return 
     */
    @WebMethod(operationName = "add")
    public int add(@WebParam(name = "operand1") int operand1, @WebParam(name = "operand2") int operand2) {
        return operand1+operand2;
    }
}
