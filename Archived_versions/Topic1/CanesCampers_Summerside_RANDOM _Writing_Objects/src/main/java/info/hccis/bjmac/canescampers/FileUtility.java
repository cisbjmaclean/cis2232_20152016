package info.hccis.bjmac.canescampers;

import static info.hccis.bjmac.canescampers.CamperMain.FILE_NAME;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bjmaclean
 */
public class FileUtility {
//
//    This code will allow us to write objects to and from a file.  Take an object and give them a try.

/**
     * This method will write an object to a file.  
     * 
     * @param objectToWrite 
     */
    public static void writeObjectToFile(Object outputToWrite, String pathLocation){
        //String pathLocation = "c:\\test\\object.ser";
        FileSystem fs = FileSystems.getDefault();
        Path path = fs.getPath(pathLocation);
 
        
         FileOutputStream fileOut=null;
            try {
                fileOut = new FileOutputStream(pathLocation);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
            }
         ObjectOutputStream out=null;
            try {
                out = new ObjectOutputStream(fileOut);
            } catch (IOException ex) {
                Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                out.writeObject(outputToWrite);
            } catch (IOException ex) {
                Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                out.close();
            } catch (IOException ex) {
                Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                fileOut.close();
            } catch (IOException ex) {
                Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

     /**
     * This method will read an object from a file.

     */
    public static Object readObjectFromFile(String pathLocation) throws FileNotFoundException, IOException, ClassNotFoundException{
         //String pathLocation = "c:\\test\\object.ser";
         HashMap<String, Camper> readObject;
         FileInputStream fileIn = new FileInputStream(pathLocation);
         ObjectInputStream in = new ObjectInputStream(fileIn);
         readObject = (HashMap<String, Camper>) in.readObject();
         in.close();
         fileIn.close();
         return readObject;
    }
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public static String readARecord(int recordNumber, String fileName) {
        String theCamperString = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            char[] chars = new char[Camper.RECORD_LENGTH];
            try {
                int offset = (recordNumber - 1) * Camper.RECORD_LENGTH;
                br.skip(offset);
                br.read(chars, 0, Camper.RECORD_LENGTH);
                theCamperString = new String(chars);
            } catch (IOException ex) {
                Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
        }

        return theCamperString;
    }

    public static boolean updateRecord(Camper camper, String fileName) {
        try {

//            //Will use FileWriter, BufferedWriter, and PrintWriter in this method.
//            FileWriter fileWriter = null;
//            try {
//                fileWriter = new FileWriter(fileName, true);
//            } catch (Exception e) {
//                System.out.println("Error creating fileWriter in FileUtility.write(...)");
//            }
//            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
//            bufferedWriter.write(camper.getDetailsForRandom(), Camper.RECORD_LENGTH*(camper.getRegistrationId()-1), Camper.RECORD_LENGTH);
//            bufferedWriter.flush();
            FileChannel fc = null;
            Path file = Paths.get(fileName);

            fc = (FileChannel) Files.newByteChannel(file, READ, WRITE);

            /*
             Go to the place in the file where this camper registration id 
             data is stored.  Starts at byte 0.
             */
            fc.position((camper.getRegistrationId() - 1) * Camper.RECORD_LENGTH);
            fc.write(ByteBuffer.wrap(camper.getDetailsForRandom().getBytes()));

        } catch (IOException ex) {
            Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }

    /**
     * This method will create the directories and the file basee on the
     * fileName provided.
     *
     * @param fileName
     * @author BJ MacLean
     * @since 20150918
     */
    public static boolean createFile(String fileName) {

        try {

            // create File object
            Path path = Paths.get(fileName);
            Files.createDirectories(path.getParent());
            Files.createFile(path);
            return true;
        } catch (IOException ex) {
            System.out.println("Note:  File already exists.");
            return false;
        }

    }

    /**
     * Initialize the random access file to blank records.
     *
     * @param fileName
     * @return
     * @since 20150918
     * @author BJ MacLean
     */
    public static void intializeFile(String fileName) {

        for (int i = 0; i < Camper.NUMBER_OF_CAMPERS; i++) {
            /* 
             Want to initialize the registration ids here starting at 
             registration id = 1.
             */

            //Build the registration id string.
            String actualNumberString = String.valueOf(i + 1);
            int lengthOfActualString = actualNumberString.length();
            String registrationIdString = "";
            //Add 9 for start of the registration id
            for (int i2 = 0; i2 < (Camper.LENGTH_REGISTRATION_ID - lengthOfActualString); i2++) {
                registrationIdString += "0";
            }
            registrationIdString += actualNumberString;
            String recordToWrite = registrationIdString + Camper.RECORD_LAYOUT.substring(Camper.LENGTH_REGISTRATION_ID);

            writeToFile(recordToWrite, FILE_NAME);
        }

    }

    static void writeLineToFile(String theCamperDetails, String FILE_NAME) {

        //Add a line feed
        theCamperDetails += System.lineSeparator();
        writeToFile(theCamperDetails, FILE_NAME);
    }

    static void writeToFile(String theCamperDetails, String FILE_NAME) {

        try {
            //Add a line feed
            Files.write(Paths.get(FILE_NAME), theCamperDetails.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException ex) {
            Logger.getLogger(CamperMain.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
