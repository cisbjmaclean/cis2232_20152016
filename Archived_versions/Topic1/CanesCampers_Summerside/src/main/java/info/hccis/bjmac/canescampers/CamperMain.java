package info.hccis.bjmac.canescampers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class will control the application for camper administration with file
 * i/o.
 *
 * @author bjmaclean
 * @since 20150918
 */
public class CamperMain {

    public static String MENU = "A) Add camper\nS) Show campers \nX)) Exit \n>>";
    public static ArrayList<Camper> campers = new ArrayList();
    public static final String FILE_NAME = "/test/testfile.txt";

    public static void main(String[] args) {

        System.out.println("Welcome to the camper administration app.");
        String option;

        FileUtility.createFile(FILE_NAME);

        /* 
        Load the campers from the file.
        */
        
        loadCampers();
        
        
        
        do {
            System.out.print(MENU);
            option = Utility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    System.out.println("Add chosen");
                    Camper newCamper = new Camper();
                    campers.add(newCamper);
                    FileUtility.writeToFile(newCamper.getCSV(), FILE_NAME);
                    break;
                case "S":
                    System.out.println("Show chosen");
                    for (Camper theCamper : campers) {
                        System.out.println(theCamper);
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");

            }

        } while (!option.equalsIgnoreCase("X"));
    }

    public static void loadCampers(){
        ArrayList<String> theStrings = FileUtility.getLinesFromFile(FILE_NAME);
        for(String nextString: theStrings){
            String[] parts = nextString.split(",");
            Camper nextCamper = new Camper(Integer.parseInt(parts[0]), parts[1], parts[2], parts[3]);
            campers.add(nextCamper);
        }
    }
    
}
