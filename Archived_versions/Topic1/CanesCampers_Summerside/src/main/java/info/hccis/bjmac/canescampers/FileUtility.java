package info.hccis.bjmac.canescampers;

import static info.hccis.bjmac.canescampers.CamperMain.FILE_NAME;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bjmaclean
 */
public class FileUtility {

    /**
     * This method will create the directories and the file basee on the
     * fileName provided.
     *
     * @param fileName
     * @author BJ MacLean
     * @since 20150918
     */
    public static void createFile(String fileName) {

        try {

            // create File object
            Path path = Paths.get(fileName);
            Files.createDirectories(path.getParent());
            Files.createFile(path);
        } catch (IOException ex) {
            System.out.println("Note:  File already exists.");
        }

    }

    /**
     * return the lines from a file as a String arrayList...
     *
     * @param fileName
     * @return
     * @since 20150918
     * @author BJ MacLean
     */
    public static ArrayList<String> getLinesFromFile(String fileName) {

        ArrayList<String> theLines = null;
        try {
            //Also add the camper to the file.
            theLines = (ArrayList<String>) Files.readAllLines(Paths.get(fileName));
            // System.out.println("There are "+theLines.size()+" entries in the file");
        } catch (IOException ex) {
            Logger.getLogger(CamperMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        return theLines;

    }

    static void writeToFile(String theCamperDetails, String FILE_NAME) {

            try {
                //Add a line feed
                theCamperDetails+=System.lineSeparator();
                Files.write(Paths.get(FILE_NAME), theCamperDetails.getBytes(), StandardOpenOption.APPEND);
            } catch (IOException ex) {
                Logger.getLogger(CamperMain.class.getName()).log(Level.SEVERE, null, ex);
            }

    }

}
