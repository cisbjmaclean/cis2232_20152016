package info.hccis.bjmac.canescampers;

/**
 * This java class will represent the bio of a camper
 * @author bjmaclean
 * @since 20150918
 */
public class Camper {
    
    private int registrationId;
    private String firstName;
    private String lastName;
    private String dob;
    private static int maxRegistrationId;
    
    
    public Camper() {
        System.out.print("enter first name >>");
        firstName = Utility.getInput().nextLine();
        System.out.print("enter last name >>");
        lastName = Utility.getInput().nextLine();
        System.out.print("enter dob (yyyymmdd) >>");
        dob = Utility.getInput().nextLine();
        registrationId = ++maxRegistrationId;
    }

    /**
     * ....
     * @param registrationId
     * @param firstName
     * @param lastName
     * @param dob 
     */
    public Camper(int registrationId, String firstName, String lastName, String dob) {
        this.registrationId = registrationId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        if(registrationId>maxRegistrationId){
            maxRegistrationId = registrationId;
        }
    }
    
    
    
    public int getRegistrationId() {
        return registrationId;
    }

    public static int getMaxRegistrationId() {
        return maxRegistrationId;
    }
    
    public void setRegistrationId(int registrationId) {
        this.registrationId = registrationId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @Override
    public String toString() {
        return "Camper:  " + registrationId + System.lineSeparator()+"First Name:  " + firstName + "\nLast Name:  " + lastName + "\ndob:  " + dob + "\n";
    }
    
public String getCSV(){
    return registrationId+","+firstName+","+lastName+","+dob;
}    
    
}
