package info.hccis.bjmac.canescampers;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class will control the application for camper administration with file
 * i/o.
 *
 * @author bjmaclean
 * @since 20150918
 */
public class CamperMain {

    public static String MENU = "A) Add/Update camper\nS) Show campers \nX)) Exit \n>>";
    //Note using a hashmap - do not want duplicates.
    public static HashMap<String, Camper> campers = new HashMap();
    public static final String FILE_NAME = "/test/testfile.txt";

    public static void main(String[] args) {

        System.out.println("Welcome to the camper administration app.");
        String option;

        if (FileUtility.createFile(FILE_NAME)) {
            //file did not exist so initialize it.
            FileUtility.intializeFile(FILE_NAME);
        }

        /* 
         Load the campers from the file.
         */
        loadCampers();

        do {
            System.out.print(MENU);
            option = Utility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    System.out.println("Add/Update chosen (specify registration id if updating otherwise leave blank to add)");
                    Camper newCamper = new Camper();
                    campers.put(String.valueOf(newCamper.getRegistrationId()), newCamper);
                    FileUtility.updateRecord(newCamper, FILE_NAME);
                    break;
                case "S":
                    System.out.println("Campers:");

                    //Iterate throught the values.
                    for (Camper theCamper : campers.values()) {
                        System.out.println(theCamper);
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");

            }

        } while (!option.equalsIgnoreCase(
                "X"));
    }

    public static void loadCampers() {

        /*Want to modify this to read from the random file.*/
        for (int recordNumber = 1; recordNumber <= Camper.NUMBER_OF_CAMPERS; recordNumber++) {
            String camperStringFromRandom = FileUtility.readARecord(recordNumber, FILE_NAME);
            /*
             If there is something in the file at that location, add it to the list.
             */
            if (!camperStringFromRandom.substring(Camper.LENGTH_REGISTRATION_ID).equals(Camper.RECORD_LAYOUT.substring(Camper.LENGTH_REGISTRATION_ID))) {
                //BJM 20150924 Adding to hashmap.
                Camper theCamper = new Camper(camperStringFromRandom);
                campers.put(String.valueOf(theCamper.getRegistrationId()), theCamper);
                
            }
        }

//        ArrayList<String> theStrings = FileUtility.getLinesFromFile(FILE_NAME);
//        for(String nextString: theStrings){
//            String[] parts = nextString.split(",");
//            Camper nextCamper = new Camper(Integer.parseInt(parts[0]), parts[1], parts[2], parts[3]);
//            campers.add(nextCamper);
//        }
    }

}
