/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.cis2232example;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author bjmaclean
 */
public class MainClass {

    public static String MENU = "Options:\nA) Add camper\nS) Show campers\nX) Exit";
    public static String FILE_NAME = "/test/campers.txt";

    public static void main(String[] args) throws IOException {
        //Create a file
        FileUtility.createFile(FILE_NAME);
        String option = "";
        ArrayList<Camper> theList = new ArrayList();
        loadCampers(theList);
        do {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    //System.out.println("Picked A");
                    Camper newCamper = new Camper();
                    theList.add(newCamper);
                    FileUtility.writeLine(FILE_NAME, newCamper.getCSV());
                    break;
                case "S":
                    System.out.println("Here are the campers");
                    for(Camper camper: theList){
                        System.out.println(camper);
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }
        } while (!option.equalsIgnoreCase("x"));
    }

    /**
     * This method will load the campers from the file at the program startup.
     *
     * @param campers
     * @since 20150917
     * @author BJ MacLean
     */
    public static void loadCampers(ArrayList campers) {
        System.out.println("Loading campers from file");
        BufferedReader theReader = FileUtility.getBufferedReader(FILE_NAME);
        String line = FileUtility.readLine(theReader);
        int count = 0;
        while (line != null) {
            count++;
            campers.add(new Camper(line.split(",")));
            System.out.println(line);
            line = FileUtility.readLine(theReader);
        }
        System.out.println("Finished...Loading campers from file (Loaded "+count+" campers)");

    }
}
