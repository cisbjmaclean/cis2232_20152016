package info.hccis.bjmac.canescampers;

import java.util.ArrayList;

/**
 * This class will control the application for camper administration with file
 * i/o.
 *
 * @author bjmaclean
 * @since 20150918
 */
public class CamperMain {

    public static String MENU = "A) Add camper\nS) Show campers \nX)) Exit \n>>";
    public static ArrayList<Camper> campers = new ArrayList();
    public static final String FILE_NAME = "/test/testfile.txt";

    public static void main(String[] args) {

        System.out.println("Welcome to the camper administration app.");
        String option;

        if (FileUtility.createFile(FILE_NAME)) {
            //file did not exist so initialize it.
            FileUtility.intializeFile(FILE_NAME);
        }

        /* 
         Load the campers from the file.
         */
        loadCampers();

        do {
            System.out.print(MENU);
            option = Utility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    System.out.println("Add chosen (note this can be used to override");
                    Camper newCamper = new Camper();
                    campers.add(newCamper);
                    FileUtility.updateRecord(newCamper, FILE_NAME);
                    break;
                case "S":
                    System.out.println("Show chosen");
                    for (Camper theCamper : campers) {
                        System.out.println(theCamper);
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");

            }

        } while (!option.equalsIgnoreCase("X"));
    }

    public static void loadCampers() {

        /*Want to modify this to read from the random file.*/
        for (int recordNumber = 1; recordNumber <= Camper.NUMBER_OF_CAMPERS; recordNumber++) {
            String camperStringFromRandom = FileUtility.readARecord(recordNumber, FILE_NAME);
            /*
             If there is something in the file at that location, add it to the list.
             */
            if (!camperStringFromRandom.substring(Camper.LENGTH_REGISTRATION_ID).equals(Camper.RECORD_LAYOUT.substring(Camper.LENGTH_REGISTRATION_ID))) {
                campers.add(new Camper(camperStringFromRandom));
            }
        }

//        ArrayList<String> theStrings = FileUtility.getLinesFromFile(FILE_NAME);
//        for(String nextString: theStrings){
//            String[] parts = nextString.split(",");
//            Camper nextCamper = new Camper(Integer.parseInt(parts[0]), parts[1], parts[2], parts[3]);
//            campers.add(nextCamper);
//        }
    }

}
