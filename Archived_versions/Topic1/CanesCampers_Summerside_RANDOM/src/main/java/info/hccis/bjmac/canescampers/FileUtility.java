package info.hccis.bjmac.canescampers;

import static info.hccis.bjmac.canescampers.CamperMain.FILE_NAME;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bjmaclean
 */
public class FileUtility {

    public static String readARecord(int recordNumber, String fileName){
        String theCamperString = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            char[] chars = new char[Camper.RECORD_LENGTH];
            try {
                int offset = (recordNumber-1)*Camper.RECORD_LENGTH;
                br.skip(offset);
                br.read(chars, 0, Camper.RECORD_LENGTH);
                theCamperString = new String(chars);
            } catch (IOException ex) {
                Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
            }   
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return theCamperString;
    }
    
    public static boolean updateRecord(Camper camper, String fileName){
                try {

//            //Will use FileWriter, BufferedWriter, and PrintWriter in this method.
//            FileWriter fileWriter = null;
//            try {
//                fileWriter = new FileWriter(fileName, true);
//            } catch (Exception e) {
//                System.out.println("Error creating fileWriter in FileUtility.write(...)");
//            }
//            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
//            bufferedWriter.write(camper.getDetailsForRandom(), Camper.RECORD_LENGTH*(camper.getRegistrationId()-1), Camper.RECORD_LENGTH);
//            bufferedWriter.flush();

            FileChannel fc = null;
            Path file = Paths.get(fileName);

                fc = (FileChannel) Files.newByteChannel(file, READ, WRITE);
                fc.position(camper.getRegistrationId()*Camper.RECORD_LENGTH);
                fc.write(ByteBuffer.wrap(camper.getDetailsForRandom().getBytes()));

        } catch (IOException ex) {
            Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }
    
    
    
    /**
     * This method will create the directories and the file basee on the
     * fileName provided.
     *
     * @param fileName
     * @author BJ MacLean
     * @since 20150918
     */
    public static boolean createFile(String fileName) {
        
        try {

            // create File object
            Path path = Paths.get(fileName);
            Files.createDirectories(path.getParent());
            Files.createFile(path);
            return true;
        } catch (IOException ex) {
            System.out.println("Note:  File already exists.");
            return false;
        }
        
    }

    /**
     * Initialize the random access file to blank records.
     *
     * @param fileName
     * @return
     * @since 20150918
     * @author BJ MacLean
     */
    public static void intializeFile(String fileName) {
        
        for (int i = 0; i < Camper.NUMBER_OF_CAMPERS; i++) {
            /* 
              Want to initialize the registration ids here starting at 
              registration id = 1.
            */
            
            //Build the registration id string.
            String actualNumberString = String.valueOf(i+1);
            int lengthOfActualString = actualNumberString.length();
            String registrationIdString = ""; 
            //Add 9 for start of the registration id
            for(int i2=0; i2<(Camper.LENGTH_REGISTRATION_ID-lengthOfActualString); i2++){
                registrationIdString += "0";
            }
            registrationIdString += actualNumberString;
            String recordToWrite = registrationIdString+Camper.RECORD_LAYOUT.substring(Camper.LENGTH_REGISTRATION_ID);
            
            writeToFile(recordToWrite, FILE_NAME);
        }
        
    }
    
    static void writeLineToFile(String theCamperDetails, String FILE_NAME) {

        //Add a line feed
        theCamperDetails += System.lineSeparator();
        writeToFile(theCamperDetails, FILE_NAME);
    }
    
    static void writeToFile(String theCamperDetails, String FILE_NAME) {
        
        try {
            //Add a line feed
            Files.write(Paths.get(FILE_NAME), theCamperDetails.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException ex) {
            Logger.getLogger(CamperMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
