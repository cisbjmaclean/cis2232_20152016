package info.hccis.bjmac.canescampers;


import info.hccis.bjmac.canescampers.util.Utility;
import info.hccis.bjmac.canescampers.dao.CamperDAO;
import info.hccis.bjmac.canescampers.dao.CodeValueDAO;
import info.hccis.bjmac.canescampers.model.Camper;
import info.hccis.bjmac.canescampers.model.CodeValue;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class will control the application for camper administration with file
 * i/o.
 *
 * @author bjmaclean
 * @since 20150918
 */
public class CamperMain {

    public static String MENU = "A) Add/Update camper\nS) Show campers \nC) Show code values \nAdd) Add code value \nX)) Exit \n>>";

    public static void main(String[] args) {

        System.out.println("Welcome to the camper administration app.");
        String option;

        do {
            System.out.print(MENU);
            option = Utility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    System.out.println("Add/Update chosen (specify registration id if updating otherwise leave blank to add)");
                    Camper newCamper = new Camper();
                    //campers.put(String.valueOf(newCamper.getRegistrationId()), newCamper);
                    //FileUtility.updateRecord(newCamper, FILE_NAME);

                    try {
                        /*
                         Refactoring code for database access.
                         */
                        newCamper = CamperDAO.update(newCamper);
                    } catch (Exception ex) {
                        Logger.getLogger(CamperMain.class.getName()).log(Level.SEVERE, "Error inserting camper", ex);
                    }

                    break;
                case "C":
                    System.out.println("Enter the code type to view");
                    String codeTypeToView = Utility.getInput().nextLine().toUpperCase();
                    ArrayList<CodeValue> theCodeValues = CodeValueDAO.getCodeValues(codeTypeToView);
                    //Display the codevalues
                    for(CodeValue cv: theCodeValues){
                        System.out.println(cv.getCodeValueSequence()+"="+cv.getDescription());
                    }
                    break;
                case "S":
                    System.out.println("Campers:");

                    //Iterate throught the values.
                    for (Camper theCamper : CamperDAO.selectAll()) {
                        System.out.println(theCamper);
                    }
                    break;
                case "ADD":
                    System.out.println("Enter the code type to add");
                    String codeTypeToAdd = Utility.getInput().nextLine().toUpperCase();
                    CodeValue codeValueToAdd = new CodeValue(Integer.parseInt(codeTypeToAdd));
                    CodeValueDAO.add(codeValueToAdd);
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");

            }

        } while (!option.equalsIgnoreCase(
                "X"));
    }


}
