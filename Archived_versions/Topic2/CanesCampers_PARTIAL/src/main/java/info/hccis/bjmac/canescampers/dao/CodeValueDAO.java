package info.hccis.bjmac.canescampers.dao;

import info.hccis.bjmac.canescampers.model.CodeValue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BJ
 */
public class CodeValueDAO {
//public static void loadCodes(HttpServletRequest request){
//    
//    request.getSession().setAttribute("organizations",getCodeValues("1"));
//    request.getSession().setAttribute("statuses",getCodeValues("2"));
//    request.getSession().setAttribute("salutations",getCodeValues("3"));
//    request.getSession().setAttribute("circulation_groups",getCodeValues("4"));
//    request.getSession().setAttribute("provinces",getCodeValues("5"));
//    request.getSession().setAttribute("genders",getCodeValues("6"));
//    request.getSession().setAttribute("countries",getCodeValues("7"));
//    request.getSession().setAttribute("programs",getCodeValues("8"));
//    request.getSession().setAttribute("employment_statuses",getCodeValues("9"));
//    request.getSession().setAttribute("currencies",getCodeValues("10"));
//    request.getSession().setAttribute("practice_areas",getCodeValues("11"));
//    request.getSession().setAttribute("employment_statuses",getCodeValues("12"));
//    request.getSession().setAttribute("employment_categories",getCodeValues("13"));
//    request.getSession().setAttribute("funding_sources",getCodeValues("14"));
//    request.getSession().setAttribute("positions",getCodeValues("15"));
//    request.getSession().setAttribute("notification_types",getCodeValues("16"));
//    request.getSession().setAttribute("user_types",getCodeValues("17"));
//    
//    return;
//}    
//
//public static CodeValue getCodeValueFromSession(HttpServletRequest request, String collectionName, int codeSequenceValue){
//    
//    ArrayList<CodeValue> theList = (ArrayList<CodeValue>) request.getSession().getAttribute(collectionName);
//    boolean found = false;
//    int location = 0;
//    CodeValue theCodeValue = null;
//    while (!found && location < theList.size()){
//        if (theList.get(location).getCodeValueSequence() == codeSequenceValue){
//            found = true;
//            theCodeValue = theList.get(location);
//        }
//        location++;
//    }
//    
//    return theCodeValue;
//} 

    public static ArrayList<CodeValue> getCodeValues(String codeTypeId) {
        ArrayList<CodeValue> codes = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM `code_value` WHERE code_type_id = " + codeTypeId;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getSTring(2);
                CodeValue codeValue = new CodeValue();
                codeValue.setCodeTypeId(Integer.parseInt(codeTypeId));
                codeValue.setCodeValueSequence(rs.getInt("code_value_sequence"));
                codeValue.setDescription(rs.getString("english_description"));
                codeValue.setDescriptionShort(rs.getString("english_description_short"));
                codes.add(codeValue);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;

    }

    public static String getCodeValueDescription(int codeTypeId, int sequence) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        String description = "";
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM `code_value` WHERE code_type_id = " + codeTypeId + " and code_value_sequence = " + sequence;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                description = rs.getString("english_description");
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return description;

    }

    public static void add(CodeValue codeValueToAdd) {
        try {
            PreparedStatement ps = null;
            String sql = null;
            Connection conn = null;
            int maxID = 0;
            try {
                conn = ConnectionUtils.getConnection();

                sql = "SELECT max(code_value_sequence) FROM `code_value` WHERE code_type_id = " + codeValueToAdd.getCodeTypeId();

                ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    maxID = rs.getInt(1);
                }
            } catch (Exception e) {
                String errorMessage = e.getMessage();
                e.printStackTrace();
            }

            sql = "INSERT INTO `code_value`(`code_type_id`, `code_value_sequence`, "
                    + "`english_description`, `english_description_short`, `french_description`,"
                    + " `french_description_short`, `created_date_time`, `created_user_id`, "
                    + "`updated_date_time`, `updated_user_id`) "
                    + "VALUES (?,?,?,?,?,?,sysdate(),'admin',sysdate(),'admin')";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, codeValueToAdd.getCodeTypeId());
            ps.setInt(2, ++maxID);
            ps.setString(3, codeValueToAdd.getDescription());
            ps.setString(4, codeValueToAdd.getDescriptionShort());
            ps.setString(5, codeValueToAdd.getDescription()+"FR");
            ps.setString(6, codeValueToAdd.getDescriptionShort()+"FR");
            ps.executeUpdate();

            DbUtils.close(ps, conn);
        } catch (SQLException ex) {
            Logger.getLogger(CodeValueDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void delete(int codeTypeId, int codeValueSequence){
        //zyler
    }
    
    public static void update(){
        //dave
        
    }
    
    //Bryan - delete code type
    //Jordan - add code type
    
    //Amy - view details of a code type
    //Chris - Show all code types
    //Connor - find code type/value for a code value description
    
    
    
    
}
