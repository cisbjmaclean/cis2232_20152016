SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `camper`
--

--
-- Table structure for table `camper`
--

CREATE TABLE IF NOT EXISTS `camper` (
  `id` int(4) unsigned NOT NULL COMMENT 'Registration id',
  `first_name` varchar(10) DEFAULT NULL,
  `last_name` varchar(10) DEFAULT NULL,
  `dob` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `camper`
--
ALTER TABLE `camper`
  ADD PRIMARY KEY (`id`);
