package info.hccis.bjmac.canescampers;



/**
 * This java class will represent the bio of a camper
 *
 * @author bjmaclean
 * @since 20150918
 */
public class Camper {

    private int registrationId;
    private String firstName;
    private String lastName;
    private String dob;
    private static int maxRegistrationId;

    public static final String RECORD_LAYOUT = "                                    ";
    public static final int RECORD_LENGTH = 36;
    public static final int NUMBER_OF_CAMPERS = 100;
    public static final int LENGTH_REGISTRATION_ID = 6;
    public static final int LENGTH_FIRST_NAME = 10;
    public static final int LENGTH_LAST_NAME = 10;
    public static final int LENGTH_DOB = 10;

    /**
     * This constructor will get the components of the string from the file and
     * split it up based on the lengths and then pass this to the other
     * constructor....
     *
     * @param camperFromRandom
     * @since 20150918
     * @author BJ MacLean
     */
    public Camper(String camperFromRandom) {
        this(Integer.parseInt(camperFromRandom.substring(0, LENGTH_REGISTRATION_ID)),
                camperFromRandom.substring(LENGTH_REGISTRATION_ID, LENGTH_REGISTRATION_ID+LENGTH_FIRST_NAME),
                camperFromRandom.substring(LENGTH_REGISTRATION_ID + LENGTH_FIRST_NAME, LENGTH_REGISTRATION_ID + LENGTH_FIRST_NAME+LENGTH_LAST_NAME),
                camperFromRandom.substring(LENGTH_REGISTRATION_ID + LENGTH_FIRST_NAME + LENGTH_LAST_NAME));
        
//        int registrationId = Integer.parseInt(camperFromRandom.substring(0, LENGTH_REGISTRATION_ID));
//        String firstName = camperFromRandom.substring(LENGTH_REGISTRATION_ID, LENGTH_REGISTRATION_ID+LENGTH_FIRST_NAME);
//        String lastName = camperFromRandom.substring(LENGTH_REGISTRATION_ID + LENGTH_FIRST_NAME, LENGTH_REGISTRATION_ID + LENGTH_FIRST_NAME+LENGTH_LAST_NAME);
//        String dob = camperFromRandom.substring(LENGTH_REGISTRATION_ID + LENGTH_FIRST_NAME + LENGTH_LAST_NAME);
    }

    public Camper() {
        System.out.print("enter registration id >>");
        String registrationIdIn = Utility.getInput().nextLine();
        /* BJM 20150924
        Have to handle the situation where there is no registration id.  Will 
        treat this like a new Camper.
        */
        if(registrationIdIn.length() > 0){
            registrationId = Integer.parseInt(registrationIdIn);
        } else {
            //Have to get the next registration id
            registrationId = ++maxRegistrationId;
        }
        
        System.out.print("enter first name >>");
        firstName = Utility.getInput().nextLine();
        System.out.print("enter last name >>");
        lastName = Utility.getInput().nextLine();
        System.out.print("enter dob (yyyymmdd) >>");
        dob = Utility.getInput().nextLine();
        //registrationId = ++maxRegistrationId;--removed since  no longer used with random access.
    }

    /**
     * .... @param registrationId
     *
     * @param firstName
     * @param lastName
     * @param dob
     */
    public Camper(int registrationId, String firstName, String lastName, String dob) {

        this.registrationId = registrationId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        if (registrationId > maxRegistrationId) {
            maxRegistrationId = registrationId;
        }
    }

    public int getRegistrationId() {
        return registrationId;
    }

    public static int getMaxRegistrationId() {
        return maxRegistrationId;
    }

    public void setRegistrationId(int registrationId) {
        this.registrationId = registrationId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @Override
    public String toString() {
        return "Camper:  " + registrationId + System.lineSeparator() + "First Name:  " + firstName + "\nLast Name:  " + lastName + "\ndob:  " + dob + "\n";
    }

    public String getCSV() {
        return registrationId + "," + firstName + "," + lastName + "," + dob;
    }

    public String getDetailsForRandom() {
        String part1 = "000000"+registrationId;
        part1 = part1.substring(part1.length()-6);
        String part2 = "          "+firstName;
        part2 = part2.substring(part2.length()-10);
        String part3 = "          "+lastName;
        part3 = part3.substring(part3.length()-10);
        String part4 = "          "+dob;
        part4 = part4.substring(part4.length()-10);
        
        /*
        Good discussion about how to pad here:  http://stackoverflow.com/questions/4051887/how-to-format-a-java-string-with-leading-zero
        */
        
        return part1 +part2+part3+part4;
                
    }

}
