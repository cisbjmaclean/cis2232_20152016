Create bjmac_roomusage database
Run roomusage.sql
If desired run initDB_room.sql to load some data.  Will want to load data with concise
start and end times. This will make sure records are only loaded once.  

Notes:
Check security - is there a password on the database and in the db config in the code.