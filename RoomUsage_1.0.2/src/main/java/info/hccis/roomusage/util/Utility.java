/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.roomusage.util;

import java.util.ArrayList;
import java.util.HashMap;
import info.hccis.roomusage.model.Room;
import info.hccis.roomusage.service.ClinicService;

/**
 *
 * @author bjmaclean
 */
public class Utility {

    public static String test = "TEST";
    private static HashMap<String, Room> rooms = null;
//    private static ClinicService clinicService;

    public static void setTheRooms(ClinicService clinicService) {
        HashMap<String, Room> theRooms = Utility.getRooms();
        if (theRooms == null) {
            theRooms = new HashMap();
            ArrayList<Room> theRoomsList = new ArrayList<Room>(clinicService.findRooms());
            for (Room current : theRoomsList) {
                theRooms.put(String.valueOf(current.getId()), current);
            }
            setRooms(theRooms);
        }
    }

    public static HashMap<String, Room> getRooms() {
        return rooms;
    }

    public static void setRooms(HashMap<String, Room> rooms) {
        Utility.rooms = rooms;
    }

//    public static void setClinicService(ClinicService clinicService) {
//        if (clinicService == null) {
//            Utility.clinicService = clinicService;
//        }
//    }

}
