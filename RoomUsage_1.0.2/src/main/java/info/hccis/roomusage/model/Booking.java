package info.hccis.roomusage.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Author: Logan Noonan/Philip Parke
 * Date: November 19, 2014
 * For: CIS-2232
 * Booking
 * Model for bookings
 * Represents bookings table on database.
 */
@Entity
@Table(name="bookings")
public class Booking {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id")
    private long id;

    @NotNull
    @Column(name="booked_from")
    private String bookedFrom;

    @NotNull
    @Column(name="booked_to")
    private String bookedTo;

    @NotNull
    @Column(name="booked_by")
    private String bookedBy;

    @NotNull
    @Column(name="description")
    private String description;

    @Column(name="room_id")
    private long roomId;


    /**
     * Constructor without params and with default values.
     */
    Booking(){
    }

    /**
     * Constructor for creating a new entry.
     *
     * @param roomId
     * @param bookedFrom
     * @param bookedTo
     * @param bookedBy
     * @param description
     */
    public Booking(long roomId, String bookedFrom, String bookedTo, String bookedBy, String description) {

        this.roomId = roomId;

        if(bookedFrom.length() == 0)
            this.bookedFrom = "";
        else if(bookedFrom.length() > 255)
            this.bookedFrom = bookedFrom.substring(0, 255);
        else
            this.bookedFrom = bookedFrom;

        if(bookedTo.length() == 0)
            this.bookedTo = "";
        else if(bookedTo.length() > 255)
            this.bookedTo = bookedFrom.substring(0, 255);
        else
            this.bookedTo = bookedFrom;

        if(bookedBy.length() == 0)
            this.bookedBy = "";
        else if(bookedBy.length() > 255)
            this.bookedBy = bookedBy.substring(0, 255);
        else
            this.bookedBy = bookedBy;

        if(description.length() == 0)
            this.description = "";
        else if(description.length() > 5000)
            this.description = description.substring(0, 5000);
        else
            this.description = description;
    }

    // Getters and setters
    public long getId() {
        return id;
    }
    public long getRoomId() {
        return roomId;
    }
    public String getBookedFrom() {
        return bookedFrom;
    }
    public String getBookedTo() {
        return bookedTo;
    }
    public String getBookedBy() {
        return bookedBy;
    }
    public String getDescription() {
        return description;
    }

    /**
     * Convert the booking to a string separated by ascii formatting characters
     * @return
     */
    public String toString(){
        return String.format("ID: %d\nRoom Id: %s\nBooked From: %s\nBooked To: %s\nBooked By: %s\nDescription: %s", id, roomId, bookedFrom, bookedTo, bookedBy, description);
    }

    void update(long roomId, String bookedFrom, String bookedTo, String bookedBy, String description)
    {
        if(roomId!=0)
            this.roomId = roomId;
        if(!bookedFrom.equals(""))
            this.bookedFrom = bookedFrom;
        if(!bookedTo.equals(""))
            this.bookedTo = bookedTo;
        if(!bookedBy.equals(""))
            this.bookedBy = bookedBy;
        if(!description.equals(""))
            this.description = description;
    }

}

