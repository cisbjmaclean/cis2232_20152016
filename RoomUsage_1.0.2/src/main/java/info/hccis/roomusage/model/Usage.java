package info.hccis.roomusage.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Author: Jon Beharrell / Philip Parke
 * Date: November 27, 2014
 * For: CIS-2232
 * Room
 * Model for usages
 * Represents usages table on database.
 */

@Entity
@Table(name="usages")
public class Usage implements Comparable<Usage>{
    @Transient
    public static final int HH_START = 11;
    @Transient
    public static final int HH_END = 13;
    @Transient
    public static final int MINUTE_START = 14;
    @Transient
    public static final int MINUTE_END = 16;
    
    
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id")
    private long id;
    
    @NotNull
    @Column(name="used_from")
    private String usedFrom;

    @NotNull
    @Column(name="used_to")
    private String usedTo;
    
    @Column(name="room_id")
    private long roomId;
    
    @Transient
    private String roomNumber;

    @Transient
    private boolean filterShortUsagesFlag;
    
    @Transient
    private int length;



    /**
     * Constructor without params and with default values.
     * Require for jpa to work with the database.
     */
    public Usage(){
        this.id = 0;
        this.usedFrom = "Used from";
        this.usedTo = "Used to";
        this.roomId = 0;
    }
    
    /**
     * Constructor for creating a new usage.
     *
     * @param usedFrom
     * @param usedTo
     * @param roomId
     */
    public Usage(String usedFrom, String usedTo, long roomId) {

        // Verify that fields are not empty or too large
        // truncate if too large
        if(usedFrom.length() == 0)
            this.usedFrom = "";
        else if(usedFrom.length() > 255)
            this.usedFrom = usedFrom.substring(0, 255);
        else
            this.usedFrom = usedFrom;

        if(usedTo.length() == 0)
            this.usedTo = "";
        else if(usedTo.length() > 255)
            this.usedTo = usedTo.substring(0, 255);
        else
            this.usedTo = usedTo;
        
        this.roomId = roomId;
    }

    /**
     * Getters/setters
     */
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsedFrom() {
        return usedFrom;
    }

    public void setUsedFrom(String usedFrom) {
        this.usedFrom = usedFrom;
    }

    public String getUsedTo() {
        return usedTo;
    }

    public void setUsedTo(String usedTo) {
        this.usedTo = usedTo;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isFilterShortUsagesFlag() {
        return filterShortUsagesFlag;
    }

    public void setFilterShortUsagesFlag(boolean filterShortUsagesFlag) {
        this.filterShortUsagesFlag = filterShortUsagesFlag;
    }

    

    void update(String usedFrom, String usedTo, long sensorId)
    {
        if(!usedFrom.equals(""))
            this.usedFrom = usedFrom;
        if(!usedTo.equals(""))
            this.usedTo = usedTo;

        if(sensorId != -1)
            this.roomId = sensorId;
    }

    /**
     * Convert the usage to a string separated by ascii formatting characters
     * @return
     */
    public String toString(){
        return String.format("ID: %d\nUsed From: %s\nUsed To: %s\nRoom ID: %d", id, usedFrom, usedTo, roomId);
    }

    @Override
    public int compareTo(Usage o) {
        
        String thisString = this.roomNumber+" "+usedFrom;
        String thatString = o.roomNumber+" "+o.usedFrom;
        return thisString.compareTo(thatString);
    }

    
}
