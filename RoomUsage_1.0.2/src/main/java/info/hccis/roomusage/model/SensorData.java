package info.hccis.roomusage.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Author: BJ MacLea
 * Date: 2015-05-07
 * For: CIS-2232
 * Room
 * Model for sensor data
 * Direct conversion from the csv file.  
 */

@Entity
@Table(name="sensor_data")
public class SensorData {
    
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id")
    private long id;
    
    @NotNull
    @Column(name="name")
    private String name;

    @NotNull
    @Column(name="data")
    private int data;

    @NotNull
    @Column(name="record_number")
    private int recordNumber;

    @NotNull
    @Column(name="time_stamp")
    private String timeStamp;

    @NotNull
    @Column(name="processed_ind")
    private int processedIndicator;

    
    /**
     * Constructor without params and with default values.
     * Require for jpa to work with the database.
     */
    public SensorData(){

    }

    public SensorData(String name, int data, int recordNumber, String timeStamp) {
        this.name = name;
        this.data = data;
        this.recordNumber = recordNumber;
        this.timeStamp = timeStamp;
        this.processedIndicator = 0;
    }

        public SensorData(String name, String data, String recordNumber, String timeStamp) {
        this.name = name;
        this.data = Integer.parseInt(data);
        this.recordNumber = Integer.parseInt(recordNumber);
        this.timeStamp = timeStamp;
        this.processedIndicator = 0;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public int getRecordNumber() {
        return recordNumber;
    }

    public void setRecordNumber(int recordNumber) {
        this.recordNumber = recordNumber;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    
    
    @Override
    public String toString() {
        return "SensorData{" + "id=" + id + ", name=" + name + ", data=" + data + ", recordNumber=" + recordNumber + ", timeStamp=" + timeStamp + '}';
    }
    



}
