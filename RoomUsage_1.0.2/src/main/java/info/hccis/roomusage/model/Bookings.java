package info.hccis.roomusage.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Simple domain object representing a list of players. Mostly here to be used for the 'players' {@link
 * org.springframework.web.servlet.view.xml.MarshallingView}.
 *
 * @author BJ MacLean
 */
@XmlRootElement
public class Bookings {

//    private List<Player> players;
//
//    @XmlElement
//    public List<Player> getPlayerList() {
//        if (players == null) {
//            players = new ArrayList<Player>();
//        }
//        return players;
//    }

}
