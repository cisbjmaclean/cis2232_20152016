package info.hccis.roomusage.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Author: Jon Beharrell / Philip Parke
 * Date: November 27, 2014
 * For: CIS-2232
 * Room
 * Model for usages
 * Represents usages table on database.
 */

@Entity
@Table(name="sensors")
public class Sensor {
    
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id")
    private long id;
    
    @NotNull
    @Column(name="name")
    private String sensorIdentifier;

    @Column(name="room_id")
    private long roomId;


    /**
     * Constructor without params and with default values.
     * Require for jpa to work with the database.
     */
    Sensor(){

    }
    
    /**
     * Constructor for creating a new sensor.
     *
     * @param sensorIdentifier
     * @param roomId
     */
    Sensor(String sensorIdentifier, long roomId) {

        // Verify that fields are not empty or too large
        // truncate if too large
        if(sensorIdentifier.length() == 0)
            this.sensorIdentifier = "";
        else if(sensorIdentifier.length() > 255)
            this.sensorIdentifier = sensorIdentifier.substring(0, 255);
        else
            this.sensorIdentifier = sensorIdentifier;

        this.roomId = roomId;
    }

    /**
     * Getters/setters
     */
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSensorIdentifier() {
        return sensorIdentifier;
    }

    public void setSensorIdentifier(String sensorIdentifier) {
        this.sensorIdentifier = sensorIdentifier;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }


    /**
     * Update the fields of the sensor with those provided.
     * @param sensorIdentifier
     */
    void update(String sensorIdentifier, long roomId)
    {
        if(!sensorIdentifier.equals(""))
            this.sensorIdentifier = sensorIdentifier;

        this.roomId = roomId;
    }


    /**
     * Convert the usage to a string separated by ascii formatting characters
     * @return
     */
    public String toString(){
        return String.format("ID: %d\nSensor Identifier: %s\nRoom ID: %d", id, sensorIdentifier, roomId);
    }

}
