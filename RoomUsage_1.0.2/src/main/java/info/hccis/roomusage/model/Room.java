package info.hccis.roomusage.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Author: Philip Parke
 * Date: November 17, 2014
 * For: CIS-2232
 * Room
 * Model for rooms
 * Represents rooms table on database.
 */
@Entity
@Table(name="rooms")
public class Room {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id")
    private long id;

    @NotNull
    @Column(name="room_number")
    private String roomNumber;

    @NotNull
    @Column(name="campus")
    private String campus;

    @NotNull
    @Column(name="room_type")
    private String roomType;

    @NotNull
    @Column(name="seats")
    private int seats;

    @NotNull
    @Column(name="description")
    private String description;


    /**
     * Constructor without params and with default values.
     * Require for jpa to work with the database.
     */
    Room(){
        this.id = 0;
        this.roomNumber = "room number";
        this.campus = "campus";
        this.roomType = "room type";
        this.seats = 0;
        this.description = "description";
    }

    /**
     * Constructor for creating a new entry.
     *
     * @param roomNumber
     * @param campus
     * @param roomType
     * @param seats
     * @param description
     */
    Room(String roomNumber, String campus, String roomType, int seats, String description) {

        // Verify that fields are not empty or too large
        // truncate if too large
        if(roomNumber.length() == 0)
            this.roomNumber = "";
        else if(roomNumber.length() > 255)
            this.roomNumber = roomNumber.substring(0, 255);
        else
            this.roomNumber = roomNumber;

        if(campus.length() == 0)
            this.campus = "";
        else if(campus.length() > 255)
            this.campus = campus.substring(0, 255);
        else
            this.campus = campus;

        if(roomType.length() > 255)
            this.roomType = roomType.substring(0, 255);
        else if(roomType.length() == 0)
            this.roomType = "";
        else
            this.roomType = roomType;

        this.seats = seats;

        if(description.length() == 0)
            this.description = "";
        else if(description.length() > 5000)
            this.description = description.substring(0, 5000);
        else
            this.description = description;
    }

    // Getters and setters
    public long getId() {
        return id;
    }
    public String getRoomNumber() {
        return roomNumber;
    }
    public String getCampus() {
        return campus;
    }
    public String getRoomType() {
        return roomType;
    }
    public int getSeats() {
        return seats;
    }
    public String getDescription() {
        return description;
    }

    /**
     * Convert the room to a string separated by ascii formatting characters
     * @return
     */
    public String toString(){
        return String.format("ID: %d\nRoom Number: %s\nCampus: %s\nRoom Type: %s\nSeats: %d\nDescription: %s", id, roomNumber, campus, seats, seats, description);
    }

    /**
     * Update the fields of the room with those provided.
     * @param roomNumber
     * @param campus
     * @param roomType
     * @param seats
     * @param description
     */
    public void update(String roomNumber, String campus, String roomType, int seats, String description)
    {
        if(!roomNumber.equals(""))
            this.roomNumber = roomNumber;
        if(!campus.equals(""))
            this.campus = campus;
        if(!roomType.equals(""))
            this.roomType = roomType;
        if(seats != -1)
            this.seats = seats;
        if(!description.equals(""))
            this.description = description;
    }


}

