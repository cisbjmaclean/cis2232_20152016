package info.hccis.roomusage.bo.usage;

import java.util.Collection;

/**
 * This class will contain business functionality related to usages.  Currently this functionality is 
 * contained in the controllers but to increase reusability it could be added in here.
 * 
 * @author bjmaclean
 * @since 20150520
 */
public class UsageBO {
    
    /**
     * This method will get the usage representation as a two dimensional array 
     * representing date/times with 1 indicating in use and 0 indicating not in use.
     * The usages for the room during the timeframe will be used to determine the 
     * on/off of each time.  
     * 
     * Initially 15 minute intervals will be used.
     * Initially a 7 day span will be used.
     * 
     * These may be changed to be more dynamic in the future.
     * 
     * Note:  May use the same filter as the usage viewing but have the room and used from
     * as mandatory input (as well as the filter short usages).
     * 
     * 
     * 
     * @return 
     */
    
public int[][] getRoomReport(String startDate, Collection usages){
    
    //Get a list of the usages ordered by usedFrom.
    /* pseudocode
    
    for each day...
      Starting at the start time (8am and for each 15 minute interval, determine 
      if the room is in use.  Use findByRoomIdAndUsedFromBetween to determine if 
      there is a usage at the given interval.  If there is then set the appropriate 
      flag in the array.
      
      
    
    
    */
    
    
    
    return null;
}    
    
    
}
