/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.hccis.roomusage.service;

import java.util.Collection;
import java.util.List;

import org.springframework.dao.DataAccessException;
import info.hccis.roomusage.model.Booking;
import info.hccis.roomusage.model.Room;
import info.hccis.roomusage.model.Usage;
import info.hccis.roomusage.model.SensorData;


/**
 * Mostly used as a facade for  controllers
 *
 * @author Michael Isvy
 */
public interface ClinicService {

    //Booking
    public List<Booking> findByBookedFromBetweenAndBookedToBetween(String fromDate1, String toDate1, String fromDate2, String toDate2);
    //public Collection<Booking> findBookingByRoomId(long id);

    //Usage
    public List<Usage> findByUsedFromBetweenAndUsedToBetween(String fromDate1, String toDate1, String fromDate2, String toDate2);
    public List<Usage> findByUsedFromBetween(String fromDate1, String toDate1);
    public List<Usage> findByRoomIdAndUsedFromBetween(long roomId, String fromDate1, String toDate1);
    public void saveUsage(Usage usage) throws DataAccessException;

    //SensorData
    public Collection<SensorData> findSensorData() throws DataAccessException;
    public void saveSensorData(SensorData sensorData) throws DataAccessException;

    //Room
    public Collection<Room> findRooms() throws DataAccessException;

}
