/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.hccis.roomusage.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import info.hccis.roomusage.model.Booking;
import info.hccis.roomusage.model.Room;
import info.hccis.roomusage.model.SensorData;
import info.hccis.roomusage.model.Usage;
import info.hccis.roomusage.repository.BookingRepository;
import info.hccis.roomusage.repository.RoomRepository;
import info.hccis.roomusage.repository.SensorDataRepository;
import info.hccis.roomusage.repository.SensorRepository;
import info.hccis.roomusage.repository.UsageRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Mostly used as a facade for all Petclinic controllers
 * Also a placeholder for @Transactional and @Cacheable annotations
 *
 * @author Michael Isvy
 */
@Service
public class ClinicServiceImpl implements ClinicService {

    private BookingRepository bookingRepository;
    private UsageRepository usageRepository;
    private SensorRepository sensorRepository;
    private RoomRepository roomRepository;
    private SensorDataRepository sensorDataRepository;

    @Autowired
    public ClinicServiceImpl(BookingRepository bookingRepository, SensorRepository sensorRepository, RoomRepository roomRepository, SensorDataRepository sensorDataRepository, UsageRepository usageRepository) {
        this.bookingRepository = bookingRepository;
        this.sensorRepository = sensorRepository;
        this.roomRepository = roomRepository;
        this.sensorDataRepository = sensorDataRepository;
        this.usageRepository = usageRepository;
    }
 
    @Override
    public List<Booking> findByBookedFromBetweenAndBookedToBetween(String fromDate1, String toDate1, String fromDate2, String toDate2) {
        return bookingRepository.findByBookedFromBetweenAndBookedToBetween(fromDate1, toDate1, fromDate2, toDate2);
    }

    @Override
    public List<Usage> findByUsedFromBetweenAndUsedToBetween(String fromDate1, String toDate1, String fromDate2, String toDate2) {
        return usageRepository.findByUsedFromBetweenAndUsedToBetween(fromDate1, toDate1, fromDate2, toDate2);
    }

    @Override
    public List<Usage> findByUsedFromBetween(String fromDate1, String toDate1) {
        return usageRepository.findByUsedFromBetween(fromDate1, toDate1);
    }
    
    
    @Override
    public List<Usage> findByRoomIdAndUsedFromBetween(long roomId, String fromDate1, String toDate1) {
        return usageRepository.findByRoomIdAndUsedFromBetween(roomId, fromDate1, toDate1);
    }

    @Override
    public Collection<SensorData> findSensorData() throws DataAccessException {
        return sensorDataRepository.findAll();
    }

    @Override
    public Collection<Room> findRooms() throws DataAccessException {
        return roomRepository.findAll();
    }

    @Override
    @Transactional
    public void saveSensorData(SensorData sensorData) throws DataAccessException {
        sensorDataRepository.save(sensorData);

    }
    
    @Override
    @Transactional
    public void saveUsage(Usage usage) throws DataAccessException {
        usageRepository.save(usage);
    }   
    
    
}
