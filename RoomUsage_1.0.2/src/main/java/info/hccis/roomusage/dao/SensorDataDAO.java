/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.roomusage.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import info.hccis.roomusage.model.SensorData;

/**
 *
 * @author bjmaclean
 */
public class SensorDataDAO {

    private final static Logger LOGGER = Logger.getLogger(SensorDataDAO.class.getName());

    public static ArrayList<SensorData> getSensorData() {
        ArrayList<SensorData> sensorRecords = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            LOGGER.log(Level.INFO, "About to get connection");
            conn = ConnectionUtils.getConnection();
            LOGGER.log(Level.INFO, "Obtained connection");

            sql = "SELECT * FROM sensor_data WHERE processed_ind = 0 and data in (0,1) order by record_number";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                LOGGER.info("Found a record, id=" + rs.getInt("id"));
                SensorData sensorData = new SensorData();
                sensorData.setId(rs.getInt("id"));
                sensorData.setName(rs.getString("name"));
                sensorRecords.add(sensorData);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return sensorRecords;
    }

    public static void processUsages() {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        ArrayList<SensorData> sensorRecords = new ArrayList();
        try {
            LOGGER.log(Level.INFO, "About to get connection");
            conn = ConnectionUtils.getConnection();
            LOGGER.log(Level.INFO, "Obtained connection");

            sql = "SELECT * FROM sensor_data WHERE processed_ind = 0 and data = '1' order by name, record_number";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                LOGGER.info("Found a record, id=" + rs.getInt("id"));
                SensorData sensorData = new SensorData();
                sensorData.setId(rs.getInt("id"));
                sensorData.setName(rs.getString("name"));
                sensorData.setData(rs.getInt("data"));
                sensorData.setRecordNumber(rs.getInt("record_number"));
                sensorData.setTimeStamp(rs.getString("time_stamp"));
                sensorRecords.add(sensorData);
            }

            //next add the usages based on the sensorRecords
            for (SensorData sensorData : sensorRecords) {
                String sensorName = "";
                int location1 = sensorData.getName().indexOf("_") + 1;
                int location2 = sensorData.getName().indexOf(" ");
                sensorName = sensorData.getName().substring(location1, location2);

                String nameFull = sensorData.getName();
                
                /*
                special case in the file where there is a sensor behind a room.  This
                10700_BEHIND 224W Occ Sensor BV Trend Archive,1,3899,2015/05/06 10:02
                In this case the room is surrounded by spaces and can also have a 
                corresponding record which does not have the behind.  This processing
                handles this situation.
                */
                
                if(nameFull.contains("BEHIND")){
                    location1 = sensorData.getName().indexOf(" ") + 1;
                    location2 = sensorData.getName().indexOf(" ",location1);
                    sensorName = sensorData.getName().substring(location1, location2);
                    sensorName += "_BEHIND";
                }
//                int locationOfSpace = name.indexOf(" ");
//                String date = name.substring(0, locationOfSpace);
//                String time = name.substring(locationOfSpace);

                /*
                Have to get the matching off record (if exists although it should exist 
                except for strange situation.  The record number should be one greater 
                than the record number of the 'ON' record.
                */
                int nextRecordNumber = sensorData.getRecordNumber()+1;
                sql = "SELECT * FROM sensor_data WHERE processed_ind = 0 and data = '0' and name='" + nameFull + "' and record_number = " + nextRecordNumber + " order by record_number";
                ps = conn.prepareStatement(sql);
                rs = ps.executeQuery();
                boolean foundMatching = false;
                String timeStampMatching = "";
                int sensorIdMatching = 0;

                /*
                 Get the matching end of usage.  This will be data=0 and recordNumber > the record number
                 for the given room number.  This could be not found because of the rooms that were in use
                 at thetime of the data extract.  
                 */
                while (rs.next() && !foundMatching) {
                    foundMatching = true; //first one found should be the one.
                    LOGGER.info("Found a TO record, id=" + rs.getInt("id"));
                    SensorData sensorDataMatching = new SensorData();
                    sensorDataMatching.setId(rs.getInt("id"));
                    sensorDataMatching.setName(rs.getString("name"));

                    sensorDataMatching.setData(rs.getInt("data"));
                    sensorDataMatching.setRecordNumber(rs.getInt("record_number"));
                    timeStampMatching = rs.getString("time_stamp");

                    /* Adjust for the 10 minute delay for the off sensor data.*/
                    int locationOfColon = timeStampMatching.indexOf(":");
                    int minutes = Integer.parseInt(timeStampMatching.substring(locationOfColon + 1, timeStampMatching.length()));
                    int hours = Integer.parseInt(timeStampMatching.substring(11, locationOfColon));
                    minutes -= 10;
                    if (minutes < 0) {
                        minutes += 60;
                        hours -= 1;
                    }
                    timeStampMatching = timeStampMatching.substring(0, 11) + hours + ":" + minutes;
                    LOGGER.info("Adjused end time to:" + timeStampMatching);
                    /* Done adjusting */
                    sensorDataMatching.setTimeStamp(timeStampMatching);

                    sensorIdMatching = rs.getInt("id");
                }

                //Get the room id from the sensor
                sql = "SELECT * FROM sensors WHERE name = '" + sensorName + "'";
                ps = conn.prepareStatement(sql);
                rs = ps.executeQuery();
                int roomId = 0;
                if (rs.next()) {
                    roomId = rs.getInt("room_id");
                } else {
                    LOGGER.info("Count not find room id for sensorName=" + sensorName);
                    sql = "INSERT INTO rooms(room_number, campus, room_type, seats, description) VALUES ('" + sensorName + "','PWC','',0,'AUTO')";
                    ps = conn.prepareStatement(sql);
                    ps.executeUpdate();

                    sql = "SELECT * FROM rooms WHERE room_number = '" + sensorName + "'";
                    ps = conn.prepareStatement(sql);
                    rs = ps.executeQuery();
                    roomId = 0;
                    if (rs.next()) {
                        roomId = rs.getInt("id");
                    }

                    //add the sensor for future use
                    sql = "INSERT INTO sensors(name, room_id) VALUES ('" + sensorName + "','" + roomId + "')";
                    ps = conn.prepareStatement(sql);
                    ps.executeUpdate();

                    LOGGER.info("Added room/sensor based on sensor_data:" + sensorName);

                }

                //insert usages record:  id, used_from, used_to, room_id
                if (timeStampMatching.equals("")) {
                    LOGGER.info("No time stamp matching found: from time=" + sensorData.getTimeStamp());
                    //sql = "insert into usages(used_from, used_to, room_id) values('" + sensorData.getTimeStamp() + "',null," + roomId + ")";
                } else {

                    sql = "insert into usages(used_from, used_to, room_id) values('" + sensorData.getTimeStamp() + "','" + timeStampMatching + "'," + roomId + ")";
                    LOGGER.info("SQL for insert=" + sql);
                    ps = conn.prepareStatement(sql);
                    ps.executeUpdate();
                }

                /*
                 Update the processed indicator if found a matching record.
                 This will indicate that this record is finalized.
                 */
                if (foundMatching) {
                    sql = "update sensor_data set processed_ind = 1 where id=" + sensorData.getId() + " or id=" + sensorIdMatching;
                    LOGGER.info("finished processing id=" + sensorData.getId() + " and " + sensorIdMatching);
                    ps = conn.prepareStatement(sql);
                    ps.executeUpdate();
                }

            }

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

    }

}

//    /**
//     * This method will insert.
//     *
//     * @return
//     * @author BJ
//     * @since 20140615
//     */
//    public static void insertNotification(Notification notification) throws Exception {
//        System.out.println("inserting notification");
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        /*
//         * Setup the sql to update or insert the row.
//         */
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "INSERT INTO `notification`(`notification_type_code`,  "
//                    + "`notification_detail`, `status_code`, `created_date_time`, "
//                    + "`created_user_id`, `updated_date_time`, `updated_user_id`) "
//                    + "VALUES (?, ?, 1, sysdate(), ?, sysdate(), ?)";
//
//            ps = conn.prepareStatement(sql);
//            ps.setInt(1, notification.getNotificationType());
//            ps.setString(2, notification.getNotificationDetail());
//            ps.setString(3, notification.getUserId());
//            ps.setString(4, notification.getUserId());
//
//            ps.executeUpdate();
//
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return;
//
//    }
//
//    /**
//     * Delete the specified member education (set to inactive)
//     * @param memberId
//     * @param memberEducationSequence 
//     */
//    public static void deleteNotification(int notificationId) throws Exception{
//        
//        System.out.println("deleting notification");
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        /*
//         * Setup the sql to update or insert the row.
//         */
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "update notification set status_code = 0, updated_date_time = sysdate() "
//                + "where notification_id = ? ";
//
//            ps = conn.prepareStatement(sql);
//            ps.setInt(1, notificationId);
//
//            ps.executeUpdate();
//
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return;
//
//    }
//        
//
//    
//    public static ArrayList<Notification> getNotifications() {
//        ArrayList<Notification> notifications = new ArrayList();
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "SELECT * FROM notification WHERE status_code = 1 order by created_date_time desc";
//
//            ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                Notification newNotification = new Notification();
//                newNotification.setNotificationId(rs.getInt("notification_id"));
//                newNotification.setNotificationDetail(rs.getString("notification_detail"));
//                newNotification.setNotificationType(rs.getInt("notification_type_code"));
//                newNotification.setNotificationDate(rs.getString("created_date_time"));
//                newNotification.setUserId(rs.getString("created_user_id"));
//                notifications.add(newNotification);
//            }
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return notifications;
//    }

