package info.hccis.roomusage.web;

import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import info.hccis.roomusage.model.Usage;
import info.hccis.roomusage.service.ClinicService;
import info.hccis.roomusage.util.Utility;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ReportController {

    private final ClinicService clinicService;
    private final static Logger LOGGER = Logger.getLogger(ReportController.class.getName());

    @Autowired
    public ReportController(ClinicService clinicService) {
        this.clinicService = clinicService;
    }
    
    public ReportController(){
        clinicService = null;
    }

    
//    @RequestMapping("/report/reportRoomWeek")
//    public String showReportRoomWeek(Model model) {
//        
//        ArrayList<Day> test = new ArrayList();
//        test.add(new Day());
//        test.add(new Day());
//        test.add(new Day());
//        
//        model.addAttribute("test",test);
//        
//        return "reports/reportRoomWeek";
//    }

    /**
     * get the input criteria for the usages.
     *
     * @return to view the appropriate usages.
     */
    @RequestMapping("/report/reportRoomWeekInput/{reportNumber}")
    public ModelAndView showReportInput(Model model,@PathVariable int reportNumber ) {

        //Ensure the rooms are set
        Utility.setTheRooms(clinicService);
        
        ModelAndView mav = new ModelAndView("usages/usageInput");
        model.addAttribute("usage", new Usage());
        LOGGER.info("Report number: "+reportNumber+" being processed");
        model.addAttribute("reportNumber", 1);
        model.addAttribute("rooms", Utility.getRooms().values());
        return mav;
        //return "players/playerList";
    }
    
    
    
    
    
    
    
    


//    @RequestMapping("/usages")
//    public String showUsageList(Usage usage, Model model) {
//
//        /*
//         Make sure that the rooms are loaded from the rooms table.  Have to load these
//         as a colletion the put them into a HashMap in the Utility class for future use.  
//         These are only loaded once.
//         */
//
//        setTheRooms();
//
//        //test the usage repository
//        LOGGER.log(Level.INFO, "about to test the usages gets");
//        LOGGER.log(Level.INFO, "from date passed in=" + usage.getUsedFrom());
//        LOGGER.log(Level.INFO, "to date passed in=" + usage.getUsedTo());
//        LOGGER.log(Level.INFO, "id passed in=" + usage.getRoomId());
//
//        String to = "";
//        String from = "";
//        boolean haveDates = false;
//        if (!usage.getUsedFrom().equals("") && !usage.getUsedFrom().equalsIgnoreCase("Used from")) {
//            haveDates = true;
//            from = usage.getUsedFrom();
//            if (usage.getUsedTo().equals("") || usage.getUsedTo().equalsIgnoreCase("Used to")) {
//                to = from + " 23:59";
//            } else {
//                to = usage.getUsedTo();
//            }
//        }
//
//        boolean haveRoomId = false;
//        long roomId = 0;
//        if (usage.getRoomId() > 0) {
//            haveRoomId = true;
//            roomId = usage.getRoomId();
//        }
//
//        Collection<Usage> usages = null;
//        if (haveDates) {
//            if (haveRoomId) {
//                LOGGER.info("Getting by room id and used from dates");
//                usages = springDataUsageRepository.findByRoomIdAndUsedFromBetween(roomId, from, to);
//            } else {
//                usages = springDataUsageRepository.findByUsedFromBetween(from, to);
//            }
//
//        } else {
//            if (haveRoomId) {
//                usages = springDataUsageRepository.findByRoomId(roomId);
//            } else {
//                usages = springDataUsageRepository.findAll();
//            }
//        }
//
//        //Want to get the room number for each usage.  
//        LOGGER.log(Level.INFO, "Testing usages, size=" + usages.size());
//
//        //Load the room number for the usages.
//        ArrayList<Usage> theUsageList = new ArrayList<Usage>(usages);
//        HashMap<String, Room> theRoomMap = Utility.getRooms();
//        for (Usage current : theUsageList) {
//
//            try {
//                current.setUsedFrom(current.getUsedFrom().substring(0, 16));
//                if (current.getUsedTo().length() > 15) {
//                    current.setUsedTo(current.getUsedTo().substring(0, 16));
//                }
//
//                int mmTo = Integer.parseInt(current.getUsedTo().substring(14));
//                int mmFrom = Integer.parseInt(current.getUsedFrom().substring(14));
//                int hhTo = Integer.parseInt(current.getUsedTo().substring(11, 13));
//                int hhFrom = Integer.parseInt(current.getUsedFrom().substring(11, 13));
//                int length = (hhTo - hhFrom) * 60 + (mmTo - mmFrom);
//                current.setLength(length);
//            } catch (Exception e) {
//                LOGGER.info("Something went wrong truncing the time");
//            }
//            String usageIdString = String.valueOf(current.getRoomId());
//            current.setRoomNumber(theRoomMap.get(usageIdString).getRoomNumber());
//        }
//
//        /*
//         If they have chosen to filter short usages, then remove them.
//         */
//        ArrayList<Usage> usagesFiltered = new ArrayList();
//        if (usage.isFilterShortUsagesFlag()) {
//            for (Usage current : theUsageList) {
//                if (current.getLength() >= 5) {
//                    usagesFiltered.add(current);
//                }
//            }
//            theUsageList = usagesFiltered;
//        }
//
//        model.addAttribute("usages", theUsageList);
//        //model.addAttribute("usages", usages);
//        LOGGER.log(Level.INFO, "About to return");
//        return "usages/usageList";
//
//    }
//
//    private void setTheRooms(){
//        HashMap<String, Room> theRooms = Utility.getRooms();
//        if (theRooms == null) {
//            theRooms = new HashMap();
//            ArrayList<Room> theRoomsList = new ArrayList<Room>(clinicService.findRooms());
//            for (Room current : theRoomsList) {
//                theRooms.put(String.valueOf(current.getId()), current);
//            }
//            Utility.setRooms(theRooms);
//        }
//    }
//    
//    /**
//     * Index Show all usages.
//     *
//     * @return
//     */
////    @RequestMapping(value="/usages", method=RequestMethod.GET)
////    public List<Usage> index(@RequestHeader(value="Authorization") String token)
////    {
////            List<Usage> usages_list = new ArrayList<Usage>();
////            for (Usage item : clinicService.findPlayers() {
////                usages_list.add(item);
////            }
////            return usages_list;
////    }
//    /**
//     * Show Show a specified usage.
//     *
//     * @param id
//     * @return the usage that was found
//     */
//    @RequestMapping(value = "/usages/{id}", method = RequestMethod.GET)
//    public List<Usage> show(@PathVariable long id) {
//        //return null;
//        LOGGER.log(Level.INFO, "About to get rooms by id");
//        List<Usage> result = springDataUsageRepository.findByRoomId(id);
//        LOGGER.log(Level.INFO, "back from findByRoomId, found " + result.size() + " usages");
//
//        return result;
//    }
//
//
//    /**
//     * Custom handler for displaying an owner.
//     *
//     * @param ownerId the ID of the owner to display
//     * @return a ModelMap with the model attributes for the view
//     */
//    @RequestMapping("/usage/{roomId}")
//    public ModelAndView showOwner(@PathVariable("roomId") long roomId) {
//        ModelAndView mav = new ModelAndView("usages/usageDetails");
//
//        LOGGER.log(Level.INFO, "About to get rooms by id");
//        List<Usage> result = springDataUsageRepository.findByRoomId(roomId);
//        LOGGER.log(Level.INFO, "back from findByRoomId, found " + result.size() + " usages");
//        mav.addObject(result.get(0));
//        //from owner:  mav.addObject(this.clinicService.findOwnerById(roomId));
//        return mav;
//    }

//
//    /**
//     * Store
//     * Create a new usage.
//     * @param usage
//     * @return the usage that was created
//     */
//    @RequestMapping(value="/usages", method=RequestMethod.POST)
//    public Usage store(@RequestBody Usage usage, @RequestHeader(value="Authorization") String token)
//    {
//        if(authService.verifyToken(token)) {
//            System.out.println(usage.getBookedFrom());
//            Usage newUsage = repository.save(usage);
//
//            return newUsage;
//        }else{
//            throw new NotAuthorizedException();
//        }
//    }
//
//    /**
//     * Update
//     * Update an existing usage.
//     * @param id
//     * @param usage
//     * @param token
//     * @return
//     */
//    @RequestMapping(value="/usages/{id}", method=RequestMethod.POST, headers = "Accept=application/json")
//    public Usage update(@PathVariable long id, @RequestBody Usage usage, @RequestHeader(value="Authorization") String token)
//    {
//
//        if(authService.verifyToken(token)) {
//            // Get the existing user
//            Usage existingUsage = repository.findOne(id);
//
//            // Update the room
//            existingUsage.update(usage.getRoomId(), usage.getBookedFrom(), usage.getBookedTo(), usage.getBookedBy(), usage.getDescription());
//
//            return repository.save(existingUsage);
//
//        }else{
//            throw new NotAuthorizedException();
//        }
//
//
//    }
//
//    /**
//     * Delete
//     * Delete an existing room.
//     * @param id
//     * @return
//     */
//    @RequestMapping(value="/usages/{id}", method=RequestMethod.DELETE)
//    public Map<String,String> delete(@PathVariable long id, @RequestHeader(value="Authorization") String token)
//    {
//        Map<String,String> response = new HashMap<>();
//        response.put("status", "");
//
//        if(authService.verifyToken(token)) {
//            // Delete the room
//            repository.delete(id);
//
//            response.put("status","OK");
//        }else{
//            throw new NotAuthorizedException();
//        }
//
//        return response;
//    }
//
//
//
//}
//

    
    
    
    
    
    
}
