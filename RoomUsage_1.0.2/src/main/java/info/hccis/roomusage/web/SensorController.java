package info.hccis.roomusage.web;

import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import info.hccis.roomusage.model.Sensor;
import info.hccis.roomusage.repository.springdatajpa.SpringDataSensorRepository;
import info.hccis.roomusage.service.ClinicService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SensorController {

    private final ClinicService clinicService;
    private final static Logger LOGGER = Logger.getLogger(SensorController.class.getName());

    @Autowired
    SpringDataSensorRepository springDataSensorRepository;

    @Autowired
    public SensorController(ClinicService clinicService) {
        this.clinicService = clinicService;
    }

    @RequestMapping("/sensors")
    public String showSensorList(Model model) {
        //test the sensor repository
        LOGGER.log(Level.INFO, "about to test the sensor gets");
        Collection<Sensor> sensors = springDataSensorRepository.findAll();
        LOGGER.log(Level.INFO, "Testing sensors, size=" + sensors.size());

        model.addAttribute("sensors", sensors);
        LOGGER.log(Level.INFO, "About to return");
        return "sensors/sensorList";

    }

//    /**
//     * Show Show a specified sensor.
//     *
//     * @param id
//     * @return the sensor that was found
//     */
//    @RequestMapping(value = "/sensors/{id}", method = RequestMethod.GET)
//    public List<Sensor> show(@PathVariable String id) {
//        //return null;
//        LOGGER.log(Level.INFO, "About to get sensors by id");
//        List<Sensor> result = springDataSensorRepository.findBySensorIdentifier(id);
//        LOGGER.log(Level.INFO, "back from findBySensorId, found " + result.size() + " sensors");
//
//        return result;
//    }

    /**
     * Custom handler for displaying an sensor.
     *
     * @param sensorIdentifier the ID of the owner to display
     * @return a ModelMap with the model attributes for the view
     */
    @RequestMapping("/sensor/{sensorIdentifier}")
    public ModelAndView showSensor(@PathVariable String sensorIdentifier) {
        ModelAndView mav = new ModelAndView("sensors/sensorDetails");

        LOGGER.log(Level.INFO, "About to get sensors by identifier");
        List<Sensor> result = springDataSensorRepository.findBySensorIdentifier(sensorIdentifier);
        LOGGER.log(Level.INFO, "back from findByRoomId, found " + result.size() + " sensors");
        mav.addObject(result.get(0));
        //from owner:  mav.addObject(this.clinicService.findOwnerById(roomId));
        return mav;
    }
}
