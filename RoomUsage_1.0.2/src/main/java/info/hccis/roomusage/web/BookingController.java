package info.hccis.roomusage.web;

import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import info.hccis.roomusage.model.Booking;
import info.hccis.roomusage.repository.springdatajpa.SpringDataBookingRepository;
import info.hccis.roomusage.service.ClinicService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BookingController {

    private final ClinicService clinicService;
    private final static Logger LOGGER = Logger.getLogger(BookingController.class.getName());

    @Autowired
    SpringDataBookingRepository springDataBookingRepository;

    @Autowired
    public BookingController(ClinicService clinicService) {
        this.clinicService = clinicService;
    }

    @RequestMapping("/bookings")
    public String showBookingList(Model model) {
        //test the booking repository
        LOGGER.log(Level.INFO, "about to test the booking gets");
        Collection<Booking> bookings = springDataBookingRepository.findAll();
        LOGGER.log(Level.INFO, "Testing bookings, size=" + bookings.size());

        model.addAttribute("bookings", bookings);
        LOGGER.log(Level.INFO, "About to return");
        return "bookings/bookingList";

    }

    /**
     * Index Show all bookings.
     *
     * @return
     */
//    @RequestMapping(value="/bookings", method=RequestMethod.GET)
//    public List<Booking> index(@RequestHeader(value="Authorization") String token)
//    {
//            List<Booking> bookings_list = new ArrayList<Booking>();
//            for (Booking item : clinicService.findPlayers() {
//                bookings_list.add(item);
//            }
//            return bookings_list;
//    }
    /**
     * Show
     * Show a specified booking.
     * @param id
     * @return the booking that was found
     */
    @RequestMapping(value="/bookings/{id}", method=RequestMethod.GET)
    public List<Booking> show(@PathVariable long id) {
          //return null;
        LOGGER.log(Level.INFO, "About to get rooms by id");
        List<Booking> result = springDataBookingRepository.findByRoomId(id);
        LOGGER.log(Level.INFO, "back from findByRoomId, found "+result.size()+" bookings");
        
        return result;
    }
    
        /**
     * Custom handler for displaying an owner.
     *
     * @param ownerId the ID of the owner to display
     * @return a ModelMap with the model attributes for the view
     */
    @RequestMapping("/booking/{roomId}")
    public ModelAndView showOwner(@PathVariable("roomId") long roomId) {
        ModelAndView mav = new ModelAndView("bookings/bookingDetails");
        
        LOGGER.log(Level.INFO, "About to get rooms by id");
        List<Booking> result = springDataBookingRepository.findByRoomId(roomId);
        LOGGER.log(Level.INFO, "back from findByRoomId, found "+result.size()+" bookings");
        mav.addObject(result.get(0));
        //from owner:  mav.addObject(this.clinicService.findOwnerById(roomId));
        return mav;
    }
    
    
//
//    /**
//     * Store
//     * Create a new booking.
//     * @param booking
//     * @return the booking that was created
//     */
//    @RequestMapping(value="/bookings", method=RequestMethod.POST)
//    public Booking store(@RequestBody Booking booking, @RequestHeader(value="Authorization") String token)
//    {
//        if(authService.verifyToken(token)) {
//            System.out.println(booking.getBookedFrom());
//            Booking newBooking = repository.save(booking);
//
//            return newBooking;
//        }else{
//            throw new NotAuthorizedException();
//        }
//    }
//
//    /**
//     * Update
//     * Update an existing booking.
//     * @param id
//     * @param booking
//     * @param token
//     * @return
//     */
//    @RequestMapping(value="/bookings/{id}", method=RequestMethod.POST, headers = "Accept=application/json")
//    public Booking update(@PathVariable long id, @RequestBody Booking booking, @RequestHeader(value="Authorization") String token)
//    {
//
//        if(authService.verifyToken(token)) {
//            // Get the existing user
//            Booking existingBooking = repository.findOne(id);
//
//            // Update the room
//            existingBooking.update(booking.getRoomId(), booking.getBookedFrom(), booking.getBookedTo(), booking.getBookedBy(), booking.getDescription());
//
//            return repository.save(existingBooking);
//
//        }else{
//            throw new NotAuthorizedException();
//        }
//
//
//    }
//
//    /**
//     * Delete
//     * Delete an existing room.
//     * @param id
//     * @return
//     */
//    @RequestMapping(value="/bookings/{id}", method=RequestMethod.DELETE)
//    public Map<String,String> delete(@PathVariable long id, @RequestHeader(value="Authorization") String token)
//    {
//        Map<String,String> response = new HashMap<>();
//        response.put("status", "");
//
//        if(authService.verifyToken(token)) {
//            // Delete the room
//            repository.delete(id);
//
//            response.put("status","OK");
//        }else{
//            throw new NotAuthorizedException();
//        }
//
//        return response;
//    }
//
//
//
//}
//
}
