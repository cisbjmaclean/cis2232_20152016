package info.hccis.roomusage.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import info.hccis.roomusage.model.Room;
import info.hccis.roomusage.repository.springdatajpa.SpringDataRoomRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

/**
 * ORIGINALLY: Author: Philip Parke Date: November 17, 2014 For: CIS-2232 Room
 * Controller Controller for the Room model
 */
@Controller
public class RoomController {

    private final static Logger LOGGER = Logger.getLogger(RoomController.class.getName());

    @Autowired
    SpringDataRoomRepository springDataRoomRepository;

    /**
     * Index Show all rooms.
     *
     * @return
     */
    @RequestMapping(value = "/rooms")
    public String showRoomList(Model model) {
        //test the booking repository
        LOGGER.log(Level.INFO, "about to test the rooms gets");
        Collection<Room> rooms = springDataRoomRepository.findAll();
        LOGGER.log(Level.INFO, "Testing size, size=" + rooms.size());

        model.addAttribute("rooms", rooms);
        LOGGER.log(Level.INFO, "About to return");
        return "rooms/roomList";
    }

//    /**
//     * Show Show a specified room.
//     *
//     * @param id
//     * @return the room that was found
//     */
//    @RequestMapping(value = "/rooms/{id}", method = RequestMethod.GET)
//    public Room show(@PathVariable long id) {
//
//        return springDataRoomRepository.findOne(id);
//
//    }
//
//    /**
//     * Store Create a new room.
//     *
//     * @param room
//     * @return the room that was created
//     */
//    @RequestMapping(value = "/rooms", method = RequestMethod.POST)
//    public Room store(@RequestBody Room room) {
//        return springDataRoomRepository.save(room);
//    }
//
//    /**
//     * Update Update an existing room.
//     *
//     * @param room
//     * @return the location of the room as a uri.
//     */
//    @RequestMapping(value = "/rooms/{id}", method = RequestMethod.POST, headers = "Accept=application/json")
//    public Room update(@PathVariable long id, @RequestBody Room room) {
//        // Get the existing user
//        Room existingRoom = springDataRoomRepository.findOne(id);
//
//        // Update the room
//        existingRoom.update(room.getRoomNumber(), room.getCampus(), room.getRoomType(), room.getSeats(), room.getDescription());
//
//        // Save the changes in the DB
//        springDataRoomRepository.save(existingRoom);
//
//        return existingRoom;
//
//    }

//    /**
//     * Delete Delete an existing room.
//     *
//     * @param id
//     * @return
//     */
//    @RequestMapping(value = "/rooms/{id}", method = RequestMethod.DELETE)
//    public void delete(@PathVariable long id) {
//        Map<String, String> response = new HashMap<String, String>();
//        response.put("status", "");
//
//        springDataRoomRepository.delete(id);
//        response.put("status", "OK");
//    }

}
