package info.hccis.roomusage.web;

import info.hccis.roomusage.dao.SensorDataDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import info.hccis.roomusage.model.FileOperation;
import info.hccis.roomusage.service.ClinicService;
import info.hccis.roomusage.util.Utility;
import org.springframework.ui.Model;

/**
 * Author: Philip Parke updated: BJ for new version. Date: December 4, 2014 For:
 * CIS-2232 File Upload Controller Manages routes for file uploads over the web.
 */
@Controller

public class FileOperationController {

    private final static Logger LOGGER = Logger.getLogger(FileOperationController.class.getName());

    private final ClinicService clinicService;

    @Autowired
    public FileOperationController(ClinicService clinicService) {
        this.clinicService = clinicService;
    }

    ImportService importService = new ImportService();

    @RequestMapping(value = "/fileOperations", method = RequestMethod.GET)
    public String initFileForm(Model model) {
        FileOperation fo = new FileOperation();
        fo.setWhichTable("Usages");
        model.addAttribute("fileOperation", fo);

        return "utility/fileOperations";
    }

    /**
     * Handle File Upload This is the upload route to be used when uploading csv
     * files that are to be loaded into the DB
     *
     * @param fileOperation
     * @param file the file posted to the route
     * @return a message indicating what happened
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String handleFileUpload(@ModelAttribute("fileOperation") FileOperation fileOperation, @RequestParam("file") MultipartFile file, Model model) {
        // LOGGER.log(Level.INFO, "Table="+table);
        LOGGER.log(Level.INFO, "in the handleFileUpload, tableName=" + fileOperation.getWhichTable());
        String table = fileOperation.getWhichTable();
        if (!file.isEmpty()) {
            try {
                String name = file.getOriginalFilename();

                byte[] bytes = file.getBytes();
                File uploadedFile = new File(name + "-uploaded");
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(uploadedFile));
                stream.write(bytes);
                stream.close();
                System.out.println("File uploaded");

                switch (table.toLowerCase()) {
//                    case "sensors":
//                        importService.addSensorsFromCsv(uploadedFile.getPath());
//                        break;

//                    case "rooms":
//                        importService.addRoomsFromCsv(uploadedFile.getPath());
//                        break;
                    case "usages":
//                        clinicService.saveSensorData(new SensorData("test",1,2,"testtimestamp"));
                        LOGGER.log(Level.INFO, "Processing usages");

                        importService.addSensorDataFromCsv(clinicService, uploadedFile.getPath());
                        Utility.setRooms(null);
                        Utility.setTheRooms(clinicService);
                        LOGGER.log(Level.INFO, "Back from processing usages");
                        break;

                    case "bookings":
                        importService.addBookingsFromCsv(uploadedFile.getPath());
                        break;
                }

                model.addAttribute("message", "File upload successful");
                return "other/notice";

            } catch (IOException ex) {
                System.out.println("IO Exception\n" + ex.getMessage());
                model.addAttribute("message", "File upload failed");
                return "other/notice";
            }
        } else {
            model.addAttribute("message", "File was empty");
            return "other/notice";

        }
    }

    /**
     * This method populates a CSV file and returns it
     *
     * @param table Database table to export
     * @return CSV file
     */
    @RequestMapping(value = "/export", method = RequestMethod.POST)
    public @ResponseBody
    FileSystemResource handleExport(@ModelAttribute("fileOperation") FileOperation fileOperation) {

        LOGGER.log(Level.INFO, "Table passed in =***" + fileOperation.getWhichTable() + "***");

        String fileName = "";

        switch (fileOperation.getWhichTable()) {
//            case "sensors":
//                fileName = importService.sensorsToCSV();
//                break;
//
//            case "rooms":
//                fileName = importService.roomsToCSV();
//                break;
//
//            case "usages":
//                fileName = importService.usagesToCSV();
//                break;

            case "bookings":
                fileName = importService.bookingsToCSV();
                break;
        }

        return new FileSystemResource(fileName);
    }
}
