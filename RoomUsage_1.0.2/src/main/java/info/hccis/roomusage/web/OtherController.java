package info.hccis.roomusage.web;

import info.hccis.roomusage.reports.Day;
import java.util.ArrayList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    @RequestMapping("/other/futureUse")
    public String showFutureUse(Model model) {
        return "other/futureUse";
    }

    @RequestMapping("/other/about")
    public String showAbout(Model model) {
        return "other/about";
    }

    @RequestMapping("/other/welcome")
    public String showWelcome(Model model) {
        System.out.println("Going through the welcome controller...");
        return "other/welcome";
    }

    
    @RequestMapping("/other/help")
    public String showHelp(Model model) {
        return "other/help";
    }
    
}
