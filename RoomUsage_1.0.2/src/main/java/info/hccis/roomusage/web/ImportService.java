package info.hccis.roomusage.web;

import info.hccis.roomusage.dao.SensorDataDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.dao.DataAccessException;
import info.hccis.roomusage.model.Booking;
import info.hccis.roomusage.model.SensorData;
import info.hccis.roomusage.repository.BookingRepository;
import info.hccis.roomusage.service.ClinicService;
import info.hccis.roomusage.util.Utility;
import org.springframework.stereotype.Component;

/**
 * Author: Philip Parke / Jon Beharrell Date: December 2, 2014 For: CIS-2232
 * Import Service Provides methods used for importing data.
 */
@Service
@Component
public class ImportService {

    private final static Logger LOGGER = Logger.getLogger(ImportService.class.getName());
//    @Autowired
//    SensorRepository sensorRepository;
//
//    @Autowired
//    RoomRepository roomRepository;
//
//    @Autowired
//    UsageRepository usageRepository;

//    private final ClinicService clinicService;
//
//    @Autowired
//    public ImportService(ClinicService clinicService) {
//        this.clinicService = clinicService;
//    }
    @Autowired
    BookingRepository bookingRepository;

//    @Autowired
//    ClinicService clinicService;
    BufferedReader br = null;
    String line = "";
    String delimiter = ",";

//    /**
//     * Add Sensors From CSV
//     *
//     * @param filename
//     */
//    public void addSensorsFromCsv(String filename){
//
//        Map<String, List<String>> records = csvToMap(filename, new String[]{"Name", "RoomId"});
//
//        // Create new records for each of the sensor ids
//        for(int i = 0; i < records.get("Name").size(); ++i){
//            // create and save the new sensor
//            System.out.println(records.get("Name").get(i)+"---"+Long.parseLong(records.get("RoomId").get(i)));
//            sensorRepository.save(new Sensor(records.get("Name").get(i), Long.parseLong(records.get("RoomId").get(i))));
//        }
//    }
//    /**
//     * Add Rooms From CSV
//     *
//     * @param filename
//     */
//    public void addRoomsFromCsv(String filename){
//
//        Map<String, List<String>> records = csvToMap(filename, new String[]{"RoomNumber", "Campus", "RoomType", "Seats", "Description"});
//
//        // Create new records for each of the room ids
//        for(int i = 0; i < records.get("RoomNumber").size(); ++i){
//            // create and save the new room
//            roomRepository.save(new Room(records.get("RoomNumber").get(i), records.get("Campus").get(i), records.get("RoomType").get(i), Integer.parseInt(records.get("Seats").get(i)), records.get("Description").get(i)));
//        }
//    }
//    /**
//     * Add Usages From CSV
//     *
//     * @param filename
//     */
//    public void addUsagesFromCsv(String filename){
//
//        Map<String, List<String>> records = csvToMap(filename, new String[]{"Name", "Data", "Timestamp"});
//        // Name = Sensor Identifier, Data = Sensor output
//
//        // Create new records based on sensor input
//        for(int i = 0; i < records.get("Name").size(); i++) {
//
//            // If sensor data is "on"
//            if(records.get("Data").get(i).equals("1")){
//
//                String nameToFind = records.get("Name").get(i);
//
//                // Looping through to find the matching sensor data ("off")
//                for(int y = i; y < records.get("Name").size()-i; y++) {
//
//                    if(records.get("Name").get(y).equals(nameToFind) && records.get("Data").get(y).equals("0")){
//
//                        // Getting needed variables to create Usage
//                        String usedFrom = convertDate(records.get("Timestamp").get(i));
//                        String usedTo = convertDate(records.get("Timestamp").get(y));
//                        Sensor sensor = sensorRepository.findBySensorIdentifier(records.get("Name").get(i)).get(0);
//                        long roomId = sensor.getRoomId();
//
//                        // Adding new usage
//                        usageRepository.save(new Usage(usedFrom, usedTo, roomId));
//                        break;
//                    }
//                }
//            }
//        }
//    }
    /**
     * Add sensor_data From CSV
     *
     * @param filename
     */
    public void addSensorDataFromCsv(ClinicService clinicService, String filename) {

        Map<String, List<String>> records = csvToMap(filename, new String[]{"Name", "Data", "RecordNumber", "Timestamp"});
        // Name = Sensor Identifier, Data = Sensor output

        // Create new records based on sensor input
        for (int i = 0; i < records.get("Name").size(); i++) {

            /*
             Want to find the max record number in the table.  Will use this to ensure
             know what records to add.  Wont have to add the records that are already 
             in the sensor_data table.
             */
            String sensorDataName = records.get("Name").get(i);
            String sensorDataData = records.get("Data").get(i);
            String sensorDataRecordNumber = records.get("RecordNumber").get(i);
            String sensorDataTime = records.get("Timestamp").get(i);

            LOGGER.log(Level.INFO, "---------------------------------------------------------------------------------");
            LOGGER.log(Level.INFO, "BJM - Name-->" + sensorDataName);
            LOGGER.log(Level.INFO, "    - Data-->" + sensorDataData);
            LOGGER.log(Level.INFO, "    - Rec#-->" + sensorDataRecordNumber);
            LOGGER.log(Level.INFO, "    - Time-->" + sensorDataTime);

            try {
                clinicService.saveSensorData(new SensorData(sensorDataName, sensorDataData, sensorDataRecordNumber, sensorDataTime));
            } catch (DataAccessException dae) {
                LOGGER.info("(Duplicate?)There was an error saving sensor data for recordNumber=" + sensorDataRecordNumber);
            }
        }
        LOGGER.info("Finished loading sensor_data, now to process usages");

        /*
         If the record number is not already in the table then add the new 
         row in the sensor_data table.  


         BJM test.  Trying things simple jdbc connectivity
         */
        SensorDataDAO.processUsages();

    }

    /**
     * This method is used when importing usages Converts the time
     *
     * @param date String representation of the date
     * @return Formatted String representation of the date
     */
    public String convertDate(String date) {
        DateFormat inFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        DateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date = outFormat.format(inFormat.parse(date));
        } catch (ParseException ex) {
            System.out.println("There was an issue parsing the data");
        }
        return date;
    }

    /**
     * Add Bookings From CSV
     *
     * @param filename
     */
    public void addBookingsFromCsv(String filename) {

        Map<String, List<String>> records = csvToMap(filename, new String[]{"RoomNumber", "BookedFrom", "BookedTo", "BookedBy", "Description"});

        // Create new records for each of the booking ids
        for (int i = 0; i < records.get("RoomNumber").size(); ++i) {
            // create and save the new booking
            bookingRepository.save(new Booking(Long.parseLong(records.get("RoomNumber").get(i)), records.get("BookedFrom").get(i), records.get("BookedTo").get(i), records.get("BookedBy").get(i), records.get("Description").get(i)));
        }
    }

    /**
     * CSV to Map Read in a CSV file and parse it into a map Specify the
     * filename to read from and the column names that should be used as map
     * keys these will be the same names used in the csv for the columns
     *
     * @param filename read from
     * @param columns map keys, column names in the csv
     * @return Populated Map
     */
    public Map<String, List<String>> csvToMap(String filename, String[] columns) {
        Map<String, List<String>> recordLists = new HashMap<String, List<String>>();

        for (String c : columns) {
            recordLists.put(c, new ArrayList<String>());
        }

        try {
            br = new BufferedReader(new FileReader(filename));

            // Get the first line which should contain the field headers
            line = br.readLine();

            List<String> headers = Arrays.asList(line.split(delimiter));

            Map<Integer, String> indexes = new HashMap<Integer, String>();
            for (String c : columns) {
                int i = headers.indexOf(c);
                if (i == -1) {
                    System.out.println(String.format("ERROR: Could not find %s column.  This column should have a title of '%s'", c, c));
                    throw new InputMismatchException(String.format("ERROR: Could not find %s column.  This column should have a title of '%s'", c, c));
                }

                indexes.put(i, c);
            }

            while ((line = br.readLine()) != null) {
                // Get the records from each line
                String[] records = line.split(delimiter);

                for (int i = 0; i < records.length; ++i) {
                    // get the column name by index
                    String cName = indexes.get(i);

                    // add the value at that index to the
                    // list with the same column name
                    recordLists.get(cName).add(records[i]);
                }
            }

        } catch (FileNotFoundException ex) {
            System.out.println("ERROR: Could not find csv file " + filename);
        } catch (IOException ex) {
            System.out.println("ERROR: IO exception\n" + ex.getMessage());
        }
        mapToCSV("Sensor", recordLists);
        return recordLists;
    }

//    /**
//     * This method creates a CSV file on the rooms stored in the database
//     *
//     * @return filename
//     */
//    public String roomsToCSV(){
//
//        Map<String, List<String>> map = new HashMap<String, List<String>>();
//
//        // Looping through columns
//        String[] columns = new String[]{"RoomNumber", "Campus", "RoomType", "Seats", "Description"};
//        for(String c : columns)
//            map.put(c, new ArrayList<String>());
//
//        // Looping through each room and adding it to the map
//        for(Room room : roomRepository.findAll()){
//
//            // Populating ArrayLists inside columns
//            map.get("RoomNumber").add(room.getRoomNumber());
//            map.get("Campus").add(room.getCampus());
//            map.get("RoomType").add(room.getRoomType());
//            map.get("Seats").add(room.getSeats()+"");
//            map.get("Description").add(room.getDescription());
//        }
//
//        // Creating CSV file
//        return mapToCSV("rooms", map);
//    }
//    /**
//     * This method creates a CSV file on the sensors stored in the database
//     *
//     * @return filename
//     */
//    public String sensorsToCSV(){
//
//        Map<String, List<String>> map = new HashMap<String, List<String>>();
//
//        // Looping through columns
//        String[] columns = new String[]{"Name", "RoomId"};
//        for(String c : columns)
//            map.put(c, new ArrayList<String>());
//
//        // Looping through each sensor and adding it to the map
//        for(Sensor sensor : sensorRepository.findAll()){
//
//            // Populating ArrayLists inside columns
//            map.get("Name").add(sensor.getSensorIdentifier());
//            map.get("RoomId").add(sensor.getRoomId()+"");
//        }
//
//        // Creating CSV file
//        return mapToCSV("sensors", map);
//    }
    /**
     * This method creates a CSV file on the bookings stored in the database
     *
     * @return filename
     */
    public String bookingsToCSV() {

        Map<String, List<String>> map = new HashMap<String, List<String>>();

        // Looping through columns
        String[] columns = new String[]{"RoomNumber", "BookedFrom", "BookedTo", "BookedBy", "Description"};
        for (String c : columns) {
            map.put(c, new ArrayList<String>());
        }

        // Looping through each booking and adding it to the map
        for (Booking booking : bookingRepository.findAll()) {

            // Populating ArrayLists inside columns
            map.get("RoomNumber").add(booking.getRoomId() + "");
            map.get("BookedFrom").add(booking.getBookedFrom());
            map.get("BookedTo").add(booking.getBookedTo());
            map.get("BookedBy").add(booking.getBookedBy());
            map.get("Description").add(booking.getDescription());
        }

        // Creating CSV file
        return mapToCSV("bookings", map);
    }

//    /**
//     * This method creates a CSV file on the usages stored in the database
//     *
//     * @return filename
//     */
//    public String usagesToCSV(){
//
//        Map<String, List<String>> map = new HashMap<String, List<String>>();
//
//        // Looping through columns
//        String[] columns = new String[]{"UsedFrom", "UsedTo", "RoomId"};
//        for(String c : columns)
//            map.put(c, new ArrayList<String>());
//
//        // Looping through each usage and adding it to the map
//        for(Usage usage : usageRepository.findAll()){
//
//            // Populating ArrayLists inside columns
//            map.get("UsedFrom").add(usage.getUsedFrom());
//            map.get("UsedTo").add(usage.getUsedTo());
//            map.get("RoomId").add(usage.getRoomId()+"");
//        }
//
//        // Creating CSV file
//        return mapToCSV("usages", map);
//    }
    /**
     * Converts given map to a CSV file
     *
     * @param type Type of data being written. Used for naming the file
     * @param map map to convert
     * @return fileName
     */
    public String mapToCSV(String type, Map<String, List<String>> map) {

        //Getting current date to append to filename
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH");
        Date date = new Date();
        String fileName = type + dateFormat.format(date) + ".csv";

        try {

            // Create new file and BufferedWriter
            File file = new File(fileName);
            file.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));

            int x = 0;

            // Looping through and writing column names
            for (String k : map.keySet()) {

                writer.write(k);

                if (x < map.size() - 1) {
                    writer.write(delimiter);
                }

                x++;
            }
            x = 1;

            writer.newLine();

            int listSize = map.values().iterator().next().size();

            // Looping through each entry and writing the data
            for (int i = 0; i < listSize; i++) { // List
                for (String k : map.keySet()) { // Columns

                    writer.write(map.get(k).get(i));

                    if (x % map.size() != 0) {
                        writer.write(delimiter);
                    }

                    x++;
                }
                writer.newLine();
            }

            writer.flush();
            writer.close();

        } catch (IOException ex) {
            System.err.println(ex);
        }

        return fileName;
    }

}
