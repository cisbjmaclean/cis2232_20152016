//package org.springframework.samples.petclinic.repository.springdatajpa;
//
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import org.springframework.samples.petclinic.repository.SensorDataRepository;
//import org.springframework.samples.petclinic.repository.SensorRepository;
//import org.springframework.samples.petclinic.model.SensorData;
//import org.springframework.stereotype.Repository;
//
///**
// * Spring Data JPA specialization of the {@link SensorRepository} interface
// *
// * @author BJ MacLean
// * @since 20150426
// */
//@Repository
//public class JpaSensorDataRepositoryImpl implements SensorDataRepository
//    
//    @PersistenceContext
//    private EntityManager em;
//    
//    @Override
//    public void save(SensorData sensorData) {
//        this.em.merge(sensorData);
//
//    }
//}
/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.hccis.roomusage.repository.springdatajpa;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.dao.DataAccessException;

import info.hccis.roomusage.model.SensorData;
import info.hccis.roomusage.repository.SensorDataRepository;
import org.springframework.stereotype.Repository;

/**
 * Using native JPA instead of Spring Data JPA here because of this query: "SELECT owner FROM Owner owner left join
 * fetch owner.pets WHERE owner.lastName LIKE :lastName" See https://jira.springsource.org/browse/DATAJPA-292 for more
 * details.
 *
 * @author Michael Isvy
 */
@Repository
public class JpaSensorDataRepositoryImpl implements SensorDataRepository {

    @PersistenceContext
    private EntityManager em;


    @Override
    @SuppressWarnings("unchecked")
    public SensorData save(SensorData sensorData) throws DataAccessException{
        this.em.merge(sensorData);
        return sensorData;
    }

    @Override
    public Collection<SensorData> findAll() throws DataAccessException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
