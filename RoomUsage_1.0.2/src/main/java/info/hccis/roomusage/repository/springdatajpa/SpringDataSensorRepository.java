package info.hccis.roomusage.repository.springdatajpa;

import org.springframework.data.repository.Repository;
import info.hccis.roomusage.model.Sensor;
import info.hccis.roomusage.repository.SensorRepository;

/**
 * Spring Data JPA specialization of the {@link SensorRepository} interface
 *
 * @author BJ MacLean
 * @since 20150426
 */
public interface SpringDataSensorRepository extends SensorRepository, Repository<Sensor, Integer> {
}
