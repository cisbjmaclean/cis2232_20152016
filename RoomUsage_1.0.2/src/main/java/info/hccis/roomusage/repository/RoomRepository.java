package info.hccis.roomusage.repository;

import java.util.Collection;
import org.springframework.dao.DataAccessException;
import info.hccis.roomusage.model.Room;

/**
 * Repository class for <code>Room</code> domain objects All method names are
 * compliant with Spring Data naming conventions so this interface can easily be
 * extended for Spring Data See here:
 * http://static.springsource.org/spring-data/jpa/docs/current/reference/html/jpa.repositories.html#jpa.query-methods.query-creation
 *
 */
public interface RoomRepository {

//    List<Room> findByRoomNumber(String roomNumber);
//
//    List<Room> findBySeats(int seats);
//
//    List<Room> findByCampus(String campus);
//
//    List<Room> findByRoomType(String roomType);

    Collection<Room> findAll() throws DataAccessException;

//    Room findOne(long id)throws DataAccessException;

//    Room save(Room room)throws DataAccessException;

    //void delete(Room room)throws DataAccessException;
}
