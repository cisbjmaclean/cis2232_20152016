package info.hccis.roomusage.repository;

import java.util.Collection;
import java.util.List;
import org.springframework.dao.DataAccessException;

import info.hccis.roomusage.model.BaseEntity;
import info.hccis.roomusage.model.Usage;

/**
 * Repository class for <code>Player</code> domain objects All method names are
 * compliant with Spring Data naming conventions so this interface can easily be
 * extended for Spring Data See here:
 * http://static.springsource.org/spring-data/jpa/docs/current/reference/html/jpa.repositories.html#jpa.query-methods.query-creation
 *
 */
public interface UsageRepository {

    /**
     * Retrieve all <code>Player</code>s from the data store.
     *
     * @return a <code>Collection</code> of <code>Player</code>s
     */
    List<Usage> findByUsedFromBetweenAndUsedToBetween(String fromDate1, String toDate1, String fromDate2, String toDate2);
    List<Usage> findByUsedFromBetween(String fromDate1, String toDate1);
    List<Usage> findByRoomIdAndUsedFromBetween(long roomId, String fromDate1, String toDate1);

    /**
     * Retrieve all <code>Player</code>s from the data store.
     *
     * @return a <code>Collection</code> of <code>Player</code>s
     */
    Collection<Usage> findAll() throws DataAccessException;
    List<Usage> findByRoomId(long roomId);
    
    /**
     * Save an <code>Booking</code> to the data store, either inserting or
     * updating it.
     *
     * @param booking the <code>Booking</code> to save
     * @see BaseEntity#isNew
     */
    void save(Usage usage) throws DataAccessException;
}
