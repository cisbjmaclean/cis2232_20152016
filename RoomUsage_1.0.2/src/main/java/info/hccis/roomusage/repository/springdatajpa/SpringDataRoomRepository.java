package info.hccis.roomusage.repository.springdatajpa;

import org.springframework.data.repository.Repository;
import info.hccis.roomusage.model.Room;
import info.hccis.roomusage.repository.RoomRepository;

/**
 * Spring Data JPA specialization of the {@link PlayerRepository} interface
 *
 * @author BJ MacLean
 * @since 20150426
 */
public interface SpringDataRoomRepository extends RoomRepository, Repository<Room, Integer> {
}
