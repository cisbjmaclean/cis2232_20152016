package info.hccis.roomusage.repository;

import java.util.Collection;
import java.util.List;
import org.springframework.dao.DataAccessException;
import info.hccis.roomusage.model.BaseEntity;
import info.hccis.roomusage.model.Sensor;

/**
 * Repository class for <code>Player</code> domain objects All method names are
 * compliant with Spring Data naming conventions so this interface can easily be
 * extended for Spring Data See here:
 * http://static.springsource.org/spring-data/jpa/docs/current/reference/html/jpa.repositories.html#jpa.query-methods.query-creation
 *
 */
public interface SensorRepository {

    /**
     * Retrieve all <code>Player</code>s from the data store.
     *
     * @return a <code>Collection</code> of <code>Player</code>s
     */
    Collection<Sensor> findAll() throws DataAccessException;

    List<Sensor> findBySensorIdentifier(String sensorIdentifier);

    /**
     * Save an <code>Booking</code> to the data store, either inserting or
     * updating it.
     *
     * @param booking the <code>Booking</code> to save
     * @see BaseEntity#isNew
     */
    void save(Sensor sensor) throws DataAccessException;
}
