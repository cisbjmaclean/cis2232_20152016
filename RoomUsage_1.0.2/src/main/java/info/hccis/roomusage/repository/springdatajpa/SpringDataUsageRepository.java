package info.hccis.roomusage.repository.springdatajpa;

import org.springframework.data.repository.Repository;
import info.hccis.roomusage.model.Usage;
import info.hccis.roomusage.repository.UsageRepository;

/**
 * Spring Data JPA specialization of the {@link PlayerRepository} interface
 *
 * @author BJ MacLean
 * @since 20150426
 */
public interface SpringDataUsageRepository extends UsageRepository, Repository<Usage, Integer> {
   // public Bookings findAll();
    
}
