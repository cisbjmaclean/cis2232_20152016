package info.hccis.roomusage.repository;

import info.hccis.roomusage.model.BaseEntity;
import java.util.Collection;
import java.util.List;
import org.springframework.dao.DataAccessException;

import info.hccis.roomusage.model.Booking;


/**
 * Repository class for <code>Player</code> domain objects All method names are
 * compliant with Spring Data naming conventions so this interface can easily be
 * extended for Spring Data See here:
 * http://static.springsource.org/spring-data/jpa/docs/current/reference/html/jpa.repositories.html#jpa.query-methods.query-creation
 *
 */
public interface BookingRepository {

    /**
     * Retrieve all <code>Player</code>s from the data store.
     *
     * @return a <code>Collection</code> of <code>Player</code>s
     */
    List<Booking> findByBookedFromBetweenAndBookedToBetween(String fromDate1, String toDate1, String fromDate2, String toDate2);
    //Collection<Booking> findAllBookings() throws DataAccessException;
    // Collection<Booking> findByRoomId(long id);

    /**
     * Retrieve all <code>Player</code>s from the data store.
     *
     * @return a <code>Collection</code> of <code>Player</code>s
     */
    Collection<Booking> findAll() throws DataAccessException;

    List<Booking> findByRoomId(long roomId);

    /**
     * Save an <code>Booking</code> to the data store, either inserting or
     * updating it.
     *
     * @param booking the <code>Booking</code> to save
     * @see BaseEntity#isNew
     */
    void save(Booking booking) throws DataAccessException;
}
