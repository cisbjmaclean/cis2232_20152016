package info.hccis.roomusage.repository.springdatajpa;

import org.springframework.data.repository.Repository;
import info.hccis.roomusage.model.Booking;
import info.hccis.roomusage.repository.BookingRepository;


/**
 * Spring Data JPA specialization of the {@link PlayerRepository} interface
 *
 * @author BJ MacLean
 * @since 20150426
 */
public interface SpringDataBookingRepository extends BookingRepository, Repository<Booking, Integer> {
   // public Bookings findAll();
}
