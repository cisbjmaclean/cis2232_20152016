package info.hccis.roomusage.repository;

import java.util.Collection;
import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.CrudRepository;
import info.hccis.roomusage.model.BaseEntity;
import info.hccis.roomusage.model.Sensor;
import info.hccis.roomusage.model.SensorData;

/**
 * Repository class for <code>Player</code> domain objects All method names are
 * compliant with Spring Data naming conventions so this interface can easily be
 * extended for Spring Data See here:
 * http://static.springsource.org/spring-data/jpa/docs/current/reference/html/jpa.repositories.html#jpa.query-methods.query-creation
 *
 */
public interface SensorDataRepository{

    /**
     * Retrieve all <code>Player</code>s from the data store.
     *
     * @return a <code>Collection</code> of <code>Player</code>s
     */

    Collection<SensorData> findAll() throws DataAccessException;
    /**
     * Save an <code>Booking</code> to the data store, either inserting or
     * updating it.
     *
     * @param sensorData
     * @param booking the <code>Booking</code> to save
     * @return 
     * @see BaseEntity#isNew
     */
    SensorData save(SensorData sensorData) throws DataAccessException;
}
