package info.hccis.roomusage.reports;

import java.util.ArrayList;

/**
 *
 * @author BJ
 */
public class Day {

    private ArrayList<Integer> timeSlots = new ArrayList();
    private String label = "";
    
    public Day(String dayDate) {
        label = dayDate.substring(5);
        
        for(int i=0; i<(24*4); ++i){
        timeSlots.add(0);            
        }
        
    }

    
    
    public ArrayList<Integer> getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(ArrayList<Integer> timeSlots) {
        this.timeSlots = timeSlots;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    
}
