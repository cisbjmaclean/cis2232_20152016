package info.hccis.roomusage.reports;

import info.hccis.roomusage.reports.Day;
import java.util.ArrayList;
import java.util.Collections;
import info.hccis.roomusage.model.Usage;

/**
 *
 * @author bjmaclean
 */
public class ReportBO {

    private static ReportContent reportContent = null;
    private static Day currentDay = null;

    public static synchronized ReportContent setupUsageReport(int reportNumber, String from, String to, String roomNumber, ArrayList<Usage> theUsages) {

        reportContent = new ReportContent();

        //Make sure sorted.  This will sort based on room number and from date of the usage (using Comparable)
        Collections.sort(theUsages);

        String currentRoom = "";
        String currentRoomHold = "";

        /*
         get the days 
         */
        String currentDate = "";
        String currentDateHold = "";
        boolean processingFirstDay = true;
        boolean processingFirstRoom = true;
        int roomCount = -1; //Start at -1 to be 0 based after the first is added.  
        
        /* The report will be made up of a collection for each room.  Each room will have usages that are 
           maintained in a collection of Days.  Each day has a label and a grid of time slots each representing 
           fifteen minute slots.  The processing of a usage will take the start time and set the appropriate 
           time slots starting at the correct time and turning on the time slots based on the length of the usage.
        */
        for (Usage usage : theUsages) {

            /*
            If we are at a new room then add the day to the report usage content (collection of days).
            */
            if (!usage.getRoomNumber().equalsIgnoreCase(currentRoom)) {
                if (!processingFirstRoom) {
                    //Add the last day from the previous room before starting the new room
                    reportContent.getRoomReportContent().get(roomCount).getReportUsageContent().add(currentDay);

                }
                processingFirstRoom = false;
                processingFirstDay = true;                      //indicate that this is the first day for the room being processed.  
                currentRoom = usage.getRoomNumber();
                currentDateHold = usage.getUsedFrom().substring(0, 10); //reset the currentDateHold for this room.
                roomCount++;                                    //Increment the room count
                reportContent.getRoomReportContent().add(new RoomReportContent());
                reportContent.getRoomReportContent().get(roomCount).setName("Room: " + currentRoom + "");
                currentDay = new Day(currentDateHold);
            }

            currentDate = usage.getUsedFrom().substring(0, 10);
            if (currentDate.equals(currentDateHold)) {
                //Still on the same date
                //process the usage for the same day.
                processThisUsage(usage);
            } else {
                //Add the current day to the collection before starting another.
//                if (!processingFirstDay) {
                    reportContent.getRoomReportContent().get(roomCount).getReportUsageContent().add(currentDay);
                    processingFirstDay = false;
                    currentDay = new Day(currentDate); //Create a new day for the next usage.  
  //              }
                //
                processThisUsage(usage);
                currentDateHold = currentDate;  //this is the new current date
            }
        }

        //Have to make sure the last room / last day is added to the collection.
        if (!processingFirstRoom) { //indicates that there were some usages and therefore
            //there would be one that has to be added still.

            reportContent.getRoomReportContent().get(roomCount).getReportUsageContent().add(currentDay);

        }

        return reportContent;
    }

    /**
     * Process the usage. For this day, the time starts at 00:00. There are 96
     * timeslots (15 min each) in the day. Take the hours*60+minutes for the
     * start and end time of the usage. Divide by 15 and this would give the
     * first time slot. Then keep processing them until the minutes are > then
     * the end time of the usage. Eazy peazy.
     *
     * @since 20150522
     * @author BJ MacLean
     */
    public static void processThisUsage(Usage usage) {
        String hhFrom = usage.getUsedFrom().substring(Usage.HH_START, usage.getUsedFrom().indexOf(":"));
        String minuteFrom = usage.getUsedFrom().substring(usage.getUsedFrom().indexOf(":")+1);

        String hhTo = usage.getUsedTo().substring(Usage.HH_START, usage.getUsedTo().indexOf(":"));
        String minuteTo = usage.getUsedTo().substring(usage.getUsedTo().indexOf(":")+1);

        int hhFromInt = Integer.parseInt(hhFrom);
        int minuteFromInt = Integer.parseInt(minuteFrom);
        int hhToInt = Integer.parseInt(hhTo);
        int minuteToInt = Integer.parseInt(minuteTo);

        int fromMinutes = hhFromInt * 60 + minuteFromInt;
        int toMinutes = hhToInt * 60 + minuteToInt;

        int timeSlotStart = fromMinutes / 15;
        int timeSlotEnd = toMinutes / 15;
        for (int i = timeSlotStart; i <= timeSlotEnd; ++i) {
            currentDay.getTimeSlots().set(i+1, 1);
        }
    }

}
