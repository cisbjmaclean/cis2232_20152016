/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.roomusage.reports;

import java.util.ArrayList;

/**
 *
 * @author BJ
 */
public class RoomReportContent {
    
    private String name;
    
    private ArrayList<Day> reportUsageContent = new ArrayList();

    public ArrayList<Day> getReportUsageContent() {
        return reportUsageContent;
    }

    public void setReportUsageContent(ArrayList<Day> reportUsageContent) {
        this.reportUsageContent = reportUsageContent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    

    
}
