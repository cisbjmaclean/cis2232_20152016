package info.hccis.roomusage.reports;

import java.util.ArrayList;

/**
 * This class will hold the information that is used to produce reports.
 *
 * @author bjmaclean
 */
public class ReportContent {

    ArrayList<RoomReportContent> roomReportContent = new ArrayList();

    public ArrayList<RoomReportContent> getRoomReportContent() {
        return roomReportContent;
    }

    public void setRoomReportContent(ArrayList<RoomReportContent> roomReportContent) {
        this.roomReportContent = roomReportContent;
    }

}
