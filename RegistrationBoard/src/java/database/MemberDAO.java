/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import beans.Member;
import exceptions.PasswordException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import util.ConnectionUtils;
import util.DbUtils;

/**
 *
 * @author BJ
 */
public class MemberDAO {

    public static void addMember(Member member) throws Exception {

        System.out.println("inserting a new member");
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        conn = ConnectionUtils.getConnection();
        try {
            sql = "SELECT max(member_id) FROM member_access";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            int nextMESequence = 1;
            if (rs.next()) {
                nextMESequence = rs.getInt(1) + 1;
            }

            sql = "INSERT INTO member "
                    + "  (`member_id`, `user_id`, `organization_type`, `status_type`, "
                    + "   `user_type`, `created_date_time`, `created_user_id`, `updated_date_time`, `updated_user_id`) "
                    + "  VALUES (?, ?, ?, ?, ?,"
                    + "  sysdate(), ?, sysdate(), ?)";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, nextMESequence);
            ps.setString(2, member.getNewUserId());
            ps.setInt(3, 1);
            ps.setInt(4, 1);
            ps.setInt(5, member.getUserType());
            ps.setString(6, member.getUserId());
            ps.setString(7, member.getUserId());
            ps.executeUpdate();

            sql = "INSERT INTO member_access "
                    + "  (member_id, user_id, password, access_string, "
                    + "   created_date_time, created_user_id, updated_date_time, updated_user_id) "
                    + "  VALUES (?, ?, ?, ?,"
                    + "  sysdate(), ?, sysdate(), ?)";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, nextMESequence);
            ps.setString(2, member.getNewUserId());
            ps.setString(3, "rboard");
            ps.setString(4, "11111111");
            ps.setString(5, member.getUserId());
            ps.setString(6, member.getUserId());
            ps.executeUpdate();

            sql = "INSERT INTO member_employment(member_id, "
                    + "employment_status_code, currency_code, practice_via_telephone_ind, "
                    + "practice_via_internet_ind, practice_in_person_ind, practice_jurisdictions) "
                    + "VALUES (?, null, null, 0, 0, 0, null)";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, nextMESequence);
            ps.executeUpdate();

            sql = "INSERT INTO member_bio (member_id, first_name, middle_name, last_name, email_address) "
                    + "VALUES (?,?,?,?,?)";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, nextMESequence);
            ps.setString(2, member.getFirstName());
            ps.setString(3, member.getMiddleName());
            ps.setString(4, member.getLastName());
            ps.setString(5, member.getEmailAddress());
            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }
        return;
    }

    public static void changePassword(int memberId, String password, String userId) throws PasswordException {

        System.out.println("updating member");
        PreparedStatement psMember = null;
        String sql = null;
        Connection conn = null;

        /*
         * Setup the sql to update or insert the row (based on update flag).
         */
        try {
            conn = ConnectionUtils.getConnection();

            sql = "UPDATE member_access "
                    + "SET password=?,"
                    + "    updated_user_id = ?, "
                    + "    updated_date_time = sysdate() "
                    + "WHERE member_id = ?";

            psMember = conn.prepareStatement(sql);
            psMember.setString(1, password);
            psMember.setString(2, userId);
            psMember.setInt(3, memberId);

            psMember.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw new PasswordException("Error");
        } finally {
            DbUtils.close(psMember, conn);
        }

        return;
    }

    public static String getAllActiveMembersEmails() {
        String emails = "";
        boolean first = true;
        for (Member member : getAllActiveMembers()) {
            if (!first) {
                emails += "; ";
            }
            first = false;
            emails += member.getEmailAddress();
        }
        return emails;
    }

    public static ArrayList<Member> getAllActiveMembers() {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        ArrayList<Member> members = new ArrayList();
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM member_bio";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Member newMember = new Member();
                newMember.setMemberId(rs.getInt("member_id"));
                newMember.setFirstName(rs.getString("first_name"));
                newMember.setMiddleName(rs.getString("middle_name"));
                newMember.setLastName(rs.getString("last_name"));
                newMember.setSalutationCode(rs.getInt("salutation_code"));
                newMember.setPreviousSurnames(rs.getString("previous_surnames"));
                newMember.setEmailToMembers(rs.getBoolean("email_to_members"));
                newMember.setEmailToBusiness(rs.getBoolean("email_to_business"));
                newMember.setEmailToGovernment(rs.getBoolean("email_to_government"));
                newMember.setEmailToPEIDAExecutive(rs.getBoolean("email_to_peida_executive"));
                newMember.setAddressLine1(rs.getString("address_1"));
                newMember.setAddressLine2(rs.getString("address_2"));
                newMember.setMunicipality(rs.getString("municipality"));
                newMember.setProvinceCode(rs.getInt("province_code"));
                newMember.setPostalCode(rs.getString("postal_code"));
                newMember.setHomePhone(rs.getString("home_phone"));
                newMember.setWorkPhone(rs.getString("work_phone"));
                newMember.setWorkPhoneExtension(rs.getString("work_phone_extension"));
                newMember.setFax(rs.getString("fax_number"));
                newMember.setEmailAddress(rs.getString("email_address"));
                newMember.setWebsite(rs.getString("website_address"));
                newMember.setDateOfBirth(rs.getString("date_of_birth"));
                newMember.setGenderCode(rs.getInt("gender_code"));
                newMember.setBilingual(rs.getBoolean("bilingual_e_f_ind"));
                newMember.setBilingualOther(rs.getString("bilingual_other"));
                newMember.setCanadianCitizen(rs.getBoolean("canadian_citizen_ind"));
                newMember.setLandedImmigrant(rs.getBoolean("landed_immigrant_ind"));
                newMember.setCountryOfOriginCode(rs.getInt("country_of_origin_code"));
                newMember.setImmigrantAuthorized(rs.getBoolean("immigrant_authorized_ind"));
                newMember.setImmigrantAuthorizedExpiryDate(rs.getString("immigrant_authorized_expiry_date"));
                members.add(newMember);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return members;
    }

    public static Member getMember(String memberId) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        Member newMember = new Member();
        newMember.setMemberId(Integer.parseInt(memberId));
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM member WHERE member_id = " + memberId;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                newMember.setUserType(rs.getInt("user_type"));
            }
            
            sql = "SELECT * FROM member_bio WHERE member_id = " + memberId;

            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                newMember.setFirstName(rs.getString("first_name"));
                newMember.setMiddleName(rs.getString("middle_name"));
                newMember.setLastName(rs.getString("last_name"));
                newMember.setSalutationCode(rs.getInt("salutation_code"));
                newMember.setPreviousSurnames(rs.getString("previous_surnames"));
                newMember.setEmailToMembers(rs.getBoolean("email_to_members"));
                newMember.setEmailToBusiness(rs.getBoolean("email_to_business"));
                newMember.setEmailToGovernment(rs.getBoolean("email_to_government"));
                newMember.setEmailToPEIDAExecutive(rs.getBoolean("email_to_peida_executive"));
                newMember.setAddressLine1(rs.getString("address_1"));
                newMember.setAddressLine2(rs.getString("address_2"));
                newMember.setMunicipality(rs.getString("municipality"));
                newMember.setProvinceCode(rs.getInt("province_code"));
                newMember.setPostalCode(rs.getString("postal_code"));
                newMember.setHomePhone(rs.getString("home_phone"));
                newMember.setWorkPhone(rs.getString("work_phone"));
                newMember.setWorkPhoneExtension(rs.getString("work_phone_extension"));
                newMember.setFax(rs.getString("fax_number"));
                newMember.setEmailAddress(rs.getString("email_address"));
                newMember.setWebsite(rs.getString("website_address"));
                newMember.setDateOfBirth(rs.getString("date_of_birth"));
                newMember.setGenderCode(rs.getInt("gender_code"));
                newMember.setBilingual(rs.getBoolean("bilingual_e_f_ind"));
                newMember.setBilingualOther(rs.getString("bilingual_other"));
                newMember.setCanadianCitizen(rs.getBoolean("canadian_citizen_ind"));
                newMember.setLandedImmigrant(rs.getBoolean("landed_immigrant_ind"));
                newMember.setCountryOfOriginCode(rs.getInt("country_of_origin_code"));
                newMember.setImmigrantAuthorized(rs.getBoolean("immigrant_authorized_ind"));
                newMember.setImmigrantAuthorizedExpiryDate(rs.getString("immigrant_authorized_expiry_date"));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return newMember;
    }

    /**
     * This method will update.
     *
     * @return
     * @author BJ
     * @since 20131202
     */
    public static void updateMember(Member member) throws Exception {
        //Have to update the member based on member id.

        System.out.println("updating member");
        PreparedStatement psMember = null;
        String sql = null;
        Connection conn = null;

        /*
         * Setup the sql to update or insert the row (based on update flag).
         */
        try {
            conn = ConnectionUtils.getConnection();

            sql = "UPDATE member_bio "
                    + "SET first_name=?,middle_name=?,last_name=?,salutation_code=?,"
                    + "previous_surnames=?,email_to_members=?,email_to_business=?,email_to_government=?,"
                    + "email_to_peida_executive=?,address_1=?,address_2=?,municipality=?,province_code=?,"
                    + "postal_code=?,home_phone=?,work_phone=?,work_phone_extension=?,fax_number=?,"
                    + "email_address=?,website_address=?,date_of_birth=?,gender_code=?,bilingual_e_f_ind=?,"
                    + "bilingual_other=?,canadian_citizen_ind=?,landed_immigrant_ind=?,country_of_origin_code=?,"
                    + "immigrant_authorized_ind=?,immigrant_authorized_expiry_date=? "
                    + "WHERE member_id = ?";

            psMember = conn.prepareStatement(sql);
            psMember.setString(1, member.getFirstName());
            psMember.setString(2, member.getMiddleName());
            psMember.setString(3, member.getLastName());
            psMember.setInt(4, member.getSalutationCode());
            psMember.setString(5, member.getPreviousSurnames());
            psMember.setBoolean(6, member.isEmailToMembers());
            psMember.setBoolean(7, member.isEmailToBusiness());
            psMember.setBoolean(8, member.isEmailToGovernment());
            psMember.setBoolean(9, member.isEmailToPEIDAExecutive());
            psMember.setString(10, member.getAddressLine1());
            psMember.setString(11, member.getAddressLine2());
            psMember.setString(12, member.getMunicipality());
            psMember.setInt(13, member.getProvinceCode());
            psMember.setString(14, member.getPostalCode());
            psMember.setString(15, member.getHomePhone());
            psMember.setString(16, member.getWorkPhone());
            psMember.setString(17, member.getWorkPhoneExtension());
            psMember.setString(18, member.getFax());
            psMember.setString(19, member.getEmailAddress());
            psMember.setString(20, member.getWebsite());
            psMember.setString(21, member.getDateOfBirth());
            psMember.setInt(22, member.getGenderCode());
            psMember.setBoolean(23, member.isBilingual());
            psMember.setString(24, member.getBilingualOther());
            psMember.setBoolean(25, member.isCanadianCitizen());
            psMember.setBoolean(26, member.isLandedImmigrant());
            psMember.setInt(27, member.getCountryOfOriginCode());
            psMember.setBoolean(28, member.isImmigrantAuthorized());
            psMember.setString(29, member.getImmigrantAuthorizedExpiryDate());
            psMember.setInt(30, member.getMemberId());

//            } else {
//                //Have to insert the new member.
//
//                sql = "INSERT INTO member(user_type, last_name, first_name, password, email, "
//                        + "phone_cell, phone_home, phone_work,"
//                        + " address, status, membership_type, member_id, membership_date) "
//                        + " values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
//
//                psMember = conn.prepareStatement(sql);
//                psMember.setInt(1, memberForm.getUserType());
//                psMember.setString(2, memberForm.getLastName());
//                psMember.setString(3, memberForm.getFirstName());
//                psMember.setString(4, memberForm.getPassword());
//                psMember.setString(5, memberForm.getEmail());
//                psMember.setString(6, memberForm.getPhoneCell());
//                psMember.setString(7, memberForm.getPhoneHome());
//                psMember.setString(8, memberForm.getPhoneWork());
//                psMember.setString(9, memberForm.getAddress());
//                psMember.setInt(10, memberForm.getStatus());
//                psMember.setInt(11, memberForm.getMemberShipType());
//                psMember.setInt(12, memberForm.getNewMemberId());
//                psMember.setDate(13, null);
//
//            }
            psMember.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(psMember, conn);
        }
        return;

    }
}
