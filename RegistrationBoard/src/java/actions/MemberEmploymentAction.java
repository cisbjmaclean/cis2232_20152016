package actions;

import beans.ChangePassword;
import beans.Member;
import beans.MemberEmployer;
import beans.MemberEmployment;
import business.MemberEducationBO;
import business.MemberEmploymentBO;
import forms.MemberEmploymentForm;
import forms.UserForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import util.Util;

/**
 * This class will process the adding of education details provided.
 *
 * @author bjmaclean
 * @since 20140615
 */
public class MemberEmploymentAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        ActionMessages messages = new ActionMessages();

        Member theMember = (Member) request.getSession().getAttribute("member");
        //First check if the user is logged in
        UserForm loggedIn = (UserForm) request.getSession().getAttribute("user");
        if (loggedIn == null || !loggedIn.isAuthenticated()) {
            messages.add("error", (new ActionMessage("label.session.expired")));
            saveMessages(request, messages);
            return mapping.findForward("login");
        }

//        messages.add("message1", (new ActionMessage("label.member.education.added")));
//        saveMessages(request, messages);
        MemberEmploymentForm memberEmploymentForm = (MemberEmploymentForm) request.getAttribute("memberEmploymentForm");
        ActionForward forwardTo = null;
        System.out.println("action=" + memberEmploymentForm.getAction());

        String memberEducationSequence = (String) request.getParameter("delete");
        System.out.println("action entered=" + memberEducationSequence);

        if (!(memberEducationSequence == null) && memberEducationSequence.length() > 0) {
            //have to delete the member employer with sequencce  which is in test.
            MemberEmploymentBO.deleteMemberEmployer(loggedIn.getMemberId(), Integer.parseInt(memberEducationSequence));
            MemberEmploymentBO.setupEmployers(request, messages, loggedIn.getMemberId());
            messages.add("message1", (new ActionMessage("label.member.employer.deleted")));
            saveMessages(request, messages);
            forwardTo = mapping.findForward("Employment");
        } else {

            if (memberEmploymentForm.getAction().equalsIgnoreCase(Util.resources.getString("label.add.employer"))) {
                MemberEmployer me = new MemberEmployer();
                me.setUserId(theMember.getUserId());
                forwardTo = mapping.findForward(util.Util.resources.getString("label.add.employer"));

            } else if (memberEmploymentForm.getAction().equalsIgnoreCase(Util.resources.getString("label.save"))) {

                //Have to save the employment information.
                MemberEmployment me = new MemberEmployment();
                BeanUtils.copyProperties(me, theMember);
                BeanUtils.copyProperties(me, memberEmploymentForm);
                me.setMemberId(theMember.getMemberId());

                try {
                    MemberEmploymentBO.updateEmployment(me);
                    messages.add("message1", (new ActionMessage("label.updated.employment")));
                } catch (Exception e) {
                    messages.add("error", (new ActionMessage("label.error.updating.information")));
                }

                forwardTo = mapping.findForward("Employment");

            } else //*************************************************************************
            // Provide a message and return to the main tile.
            //*************************************************************************
            {
                forwardTo = mapping.findForward(util.Util.resources.getString("label.member.employment"));
            }
        }
        saveMessages(request, messages);
        return forwardTo;
    }
}
