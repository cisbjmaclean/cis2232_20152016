package actions;

import beans.Member;
import beans.MemberEducation;
import business.MemberEducationBO;
import forms.MemberEducationForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

/**
 * This class will process the adding of education details
 * provided.
 *
 * @author bjmaclean
 * @since 20140615
 */
public class MemberEducationAddAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        ActionMessages messages = new ActionMessages();

        Member theMember = (Member) request.getSession().getAttribute("member");
        
        //Add the education to the database
        MemberEducation memberEducation = new MemberEducation();
        MemberEducationForm memberEducationForm = (MemberEducationForm) request.getAttribute("memberEducationForm");
        memberEducationForm.setMemberId(theMember.getMemberId());
        memberEducationForm.setUserId(theMember.getUserId());
        BeanUtils.copyProperties(memberEducation, memberEducationForm);
        
        MemberEducationBO.insertMemberEducation(memberEducation);       
        request.getSession().setAttribute("Education", MemberEducationBO.getMemberEducation(theMember.getMemberId()));
        MemberEducationBO.setupEducation(request, messages, theMember.getMemberId());
        
        messages.add("message1", (new ActionMessage("label.member.education.added")));
        saveMessages(request, messages);

        //*************************************************************************
        // Provide a message and return to the main tile.
        //*************************************************************************
        ActionForward forwardTo = mapping.findForward(util.Util.resources.getString("label.member.education"));
        return forwardTo;
    }
}
