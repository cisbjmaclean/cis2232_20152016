package actions;

import beans.Member;
import business.MemberBO;
import business.MemberEducationBO;
import forms.MemberBioForm;
import forms.MemberEducationForm;
import forms.UserForm;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

/**
 * This class will process the authentication from the login information
 * provided.
 *
 * @author bjmaclean
 * @since 20131129
 */
public class MemberEducationAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        ActionMessages messages = new ActionMessages();
        saveMessages(request, messages);

        //First check if the user is logged in
        UserForm loggedIn = (UserForm) request.getSession().getAttribute("user");
        if (loggedIn == null || !loggedIn.isAuthenticated()) {
            messages.add("error", (new ActionMessage("label.session.expired")));
            saveMessages(request, messages);
            return mapping.findForward("login");
        }

        //***************************************************************************
        // Check to see if the user chose to delete (action=delete)
        //***************************************************************************
        
        MemberEducationForm formEducation = (MemberEducationForm) request.getAttribute("memberEducationForm");
        String memberEducationSequence = (String) request.getParameter("delete");
        System.out.println("action entered=" + memberEducationSequence);

        ActionForward forwardTo = null;
        if (!(memberEducationSequence == null) && memberEducationSequence.length() > 0){
            //have to delete the member education with sequencce  which is in test.
            MemberEducationBO.deleteMemberEducation(loggedIn.getMemberId(), Integer.parseInt(memberEducationSequence));
            MemberEducationBO.setupEducation(request, messages, loggedIn.getMemberId());
            messages.add("message1", (new ActionMessage("label.member.education.deleted")));
            saveMessages(request, messages);

            forwardTo = mapping.findForward("Education");
        } else {
            forwardTo = mapping.findForward("EducationAdd");
            
        }
        
        //*************************************************************************
        // Provide a message and return to the main tile.
        //*************************************************************************
        return forwardTo;
    }
}
