package actions;

import beans.Event;
import business.ProfessionalDevelopmentBO;
import database.ProfessionalDevelopmentDAO;
import forms.EventForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import util.Mail;

/**
 * This class will provide user with feedback of their choices.
 *
 * @author Roger Myesr
 * @since 2014/06/24
 */
public class ConfirmAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        /*
         Gather Information from the request
         */

        EventForm EventForm = (EventForm) request.getAttribute("eventForm");
        
        //Create a new Event object.
        Event event = new Event();
        //Set all the variables.
        event.setDate(EventForm.getDate());
        event.setEventNum(EventForm.getEventNum());
        event.setHour(EventForm.getHour());
        event.setMemberId(EventForm.getMemberId());
        event.setPdCode(EventForm.getPdCode());
        event.setDescription(EventForm.getDescription());
        
        //Pass the event to the PDDAO which will update the database.
        ProfessionalDevelopmentDAO.updateEvent(event);
        //Give them a new JSP with feedbad that the event was added successfully.
        ProfessionalDevelopmentBO.getEvents(request, EventForm.getMemberId());
        
        return mapping.findForward("Report Results");
    }
}
