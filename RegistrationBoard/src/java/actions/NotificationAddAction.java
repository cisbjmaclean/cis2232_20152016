package actions;

import beans.Member;
import beans.Notification;
import business.MemberBO;
import business.NotificationBO;
import forms.NotificationForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import util.Mail;

/**
 * This class will process the adding of education details
 * provided.
 *
 * @author bjmaclean
 * @since 20140615
 */
public class NotificationAddAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        ActionMessages messages = new ActionMessages();

        Member theMember = (Member) request.getSession().getAttribute("member");
        
        //Add the notification to the database
        Notification notification = new Notification();
        NotificationForm notificationForm = (NotificationForm) request.getAttribute("notificationForm");
        notificationForm.setMemberId(theMember.getMemberId());
        BeanUtils.copyProperties(notification, notificationForm);
        
        NotificationBO.insertNotification(notification);       
        request.getSession().setAttribute("Notifications", NotificationBO.getNotifications());
        
        messages.add("message1", (new ActionMessage("label.notification.added")));
        saveMessages(request, messages);

        //*************************************************************************
        // Call the notification method to have notifications send.
        //*************************************************************************
        
        sendNotifications(notification);
        
        //*************************************************************************
        // Provide a message and return to the main tile.
        //*************************************************************************
        ActionForward forwardTo = mapping.findForward(util.Util.resources.getString("label.notification"));
        return forwardTo;
    }

public void sendNotifications(Notification notification){
    
    //If it is an important message, send an email to active users.
    
    if (notification.getNotificationType() == 2){
        Mail mail = new Mail(MemberBO.getAllActiveMembersEmails(), "PEIDietitians@gmail.com", "Dietitian board notification", notification.getNotificationDetail());
        mail.sendMail();
    }
}
}
