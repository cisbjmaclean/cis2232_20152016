package actions;

import beans.Member;
import database.CodeValueDAO;
import database.ProfessionalDevelopmentDAO;
import forms.UserForm;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import util.Security;
import util.Util;

/**
 * This class will process the authentication from the login information
 * provided.
 *
 * @author bjmaclean
 * @since 20131129
 */
public class LoginAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        //load the properties into a class attribute for use if desired.
        Util.resources = ResourceBundle.getBundle("properties.ApplicationResource", Locale.getDefault());
        if (Util.debugOn) {
            String value = Util.resources.getString("label.future");
            System.out.println("BJM" + value);
        }

        
        /*
         Get the loginForm from the request.  Note that the code knows from the struts-config
         that loginForm is the form associated with the authenticate action
         */
        UserForm loginForm = (UserForm) request.getAttribute("userForm");
        System.out.println("Username entered=" + loginForm.getUserId());
        System.out.println("Password entered=" + loginForm.getPassword());

        int authenticatedMemberId = Security.authenticateUser(loginForm.getUserId(), loginForm.getPassword());

        //Have to do the authentication sometime...
        if (authenticatedMemberId > 0) {
            loginForm.setMemberId(authenticatedMemberId);
            loginForm.setAuthenticated(true);
            
        }
        request.getSession().setAttribute("user", loginForm);

        ActionForward forwardTo = null;
        ActionMessages messages = new ActionMessages();

        if (loginForm.isAuthenticated()) {

            
            
            //This method will load the code values into the session.  
            CodeValueDAO.loadCodes(request);
            ProfessionalDevelopmentDAO.loadCodes(request);
            
            //load the member's information from the member id found during authentication.
            Member theMember = new Member(authenticatedMemberId);
            theMember.setUserId(loginForm.getUserId());
            theMember.setPassword(loginForm.getPassword());
            request.getSession().setAttribute("member", theMember);
            request.getSession().setAttribute("user_type", theMember.getUserType());
            
            //Writing to the file the user that logged in.
            theMember.fileWrite();

            messages.add("message1", (new ActionMessage("label.login.success")));
            
            if (theMember.getPassword().equalsIgnoreCase("rboard")){
                forwardTo = mapping.findForward("ChangePassword");
                messages.add("message1", (new ActionMessage("label.password.change.required")));
            } else {
                forwardTo = mapping.findForward("main");
            }
        } else {
            messages.add("error", (new ActionMessage("label.login.fail")));
            forwardTo = mapping.findForward("login");
        }
        saveMessages(request, messages);
        return forwardTo;
    }
}
