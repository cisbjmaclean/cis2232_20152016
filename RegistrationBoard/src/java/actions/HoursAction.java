package actions;


import forms.EventForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This action checks to make sure if hours is required that hours have been 
 * entered
 * @author Roger Myers
 * @since 2014/06/24
 */
public class HoursAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        //Gather attributes from the request
        EventForm eventForm = (EventForm) request.getAttribute("eventForm");
        //Check to make sure that hours has been set
        if (eventForm.getHour() > 0) {
            //Send information back to request
            request.setAttribute("eventForm", eventForm);
            //Send them to the confirm page.
            return mapping.findForward("pdConfirm");
        } else {
            //Send information back to the request
            request.setAttribute("eventForm", eventForm);
            //Return them to the page that asks them for the hours.
            return mapping.findForward("pdHours");
        }
    }
}