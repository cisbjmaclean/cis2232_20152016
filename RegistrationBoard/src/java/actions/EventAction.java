package actions;

import beans.ProfessionalDevelopment;
import database.ProfessionalDevelopmentDAO;
import forms.EventForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Created Action to check if hours is required. 
 * @author Roger Myers
 * @since 2014/06/24
 */
public class EventAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        /*
         This action will gather the informaiton enterd and check whether or not
         they need to enter amount of hours.
         */
        //Gather informaiton from request.
        EventForm eventForm = (EventForm) request.getAttribute("eventForm");

        //Check to see if this event requires hours to be entered.
        if (ProfessionalDevelopment.checkHours(eventForm)) {
            request.setAttribute("eventForm", eventForm);
            //Loop through the array.
            for (int x = 0; x < ProfessionalDevelopmentDAO.pds.size(); ++x) {
                //If it has the marker that requires hours forward them to pdHours.
                if (ProfessionalDevelopmentDAO.pds.get(x).getPdCode() == eventForm.getPdCode()) {
                    eventForm.setPdDescription(ProfessionalDevelopmentDAO.pds.get(x).getPdTitle());
                }
            }
            return mapping.findForward("pdHours");

        //If hours is not required forward them to pdConfirm to confirm their selection.    
        } else {
            request.setAttribute("eventForm", eventForm);
            for (int x = 0; x < ProfessionalDevelopmentDAO.pds.size(); ++x) {
                if (ProfessionalDevelopmentDAO.pds.get(x).getPdCode() == eventForm.getPdCode()) {
                    eventForm.setPdDescription(ProfessionalDevelopmentDAO.pds.get(x).getPdTitle());
                }
            }
            return mapping.findForward("pdConfirm");
        }

    }
}
