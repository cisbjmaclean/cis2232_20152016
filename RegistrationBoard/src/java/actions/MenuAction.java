package actions;

import beans.Member;
import business.MemberEducationBO;
import business.MemberEmploymentBO;
import business.ProfessionalDevelopmentBO;
import database.NotificationDAO;
import forms.EventForm;
import forms.MemberBioForm;
import forms.MenuForm;
import forms.ReportsForm;
import forms.UserForm;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import util.Util;

/**
 *
 * @author bjmaclean
 */
public class MenuAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        ActionMessages messages = new ActionMessages();

        //First check if the user is logged in
        UserForm loggedIn = (UserForm) request.getSession().getAttribute("user");
        if (loggedIn == null || !loggedIn.isAuthenticated()) {
            messages.add("error", (new ActionMessage("label.session.expired")));
            saveMessages(request, messages);
            return mapping.findForward("login");
        }

        //get the main form from the request.
        MenuForm menuForm = (MenuForm) request.getAttribute("menuForm");
        System.out.println("action" + menuForm.getAction());

        //if the user clicked a button, forward to forward specified in the button.
//        } else if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.professional.development"))) {
//            return mapping.findForward("professionalDevelopment");
//        } else if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.reports"))) {
//            return mapping.findForward("reports");
//        } else if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.notices"))) {
//            return mapping.findForward("notices");
//            
//        } else 
        Member theMember = (Member) request.getSession().getAttribute("member");

        if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.basic"))) {
            MemberBioForm theMemberForm = new MemberBioForm();
            try {
                BeanUtils.copyProperties(theMemberForm, theMember);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(MenuAction.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(MenuAction.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("memberBioForm", theMemberForm);
            //return mapping.findForward("memberBio");
        } else if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.member.education"))) {

            MemberEducationBO.setupEducation(request, messages, theMember.getMemberId());

        } else if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.member.employment"))) {

            MemberEmploymentBO.setupEmployers(request, messages, theMember.getMemberId());

        } else if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.change.password"))) {

            return mapping.findForward("ChangePassword");
        } //Menu click that goes to professional development
        else if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.professional.development"))) {

            ProfessionalDevelopmentBO.getEvents(request, theMember.getMemberId());
            return mapping.findForward("Report Results");

        } //Menu click that does to reports.
        else if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.reports"))) {
            ReportsForm reportsForm = new ReportsForm();
            //Passes the member id to the report form.
            reportsForm.setMemberId(theMember.getMemberId());
            request.setAttribute("reportForm", reportsForm);

        } else if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.notification"))) {
            request.getSession().setAttribute("Notifications", NotificationDAO.getNotifications());
        } else if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.professional.development.add"))) {

            EventForm eventForm = new EventForm();
            //Gathering the information of the user logged in and passing to
            //the event form.
            eventForm.setMemberId(theMember.getMemberId());
            eventForm.setFirstName(theMember.getFirstName());
            eventForm.setLastName(theMember.getLastName());
            request.setAttribute("eventForm", eventForm);


        } else if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.add.user"))) {

            //Send the user to add a user.
            //- add a member, member_accesss, member_employment, member_bio rows but
            //- will only need to obtain the user name, user_id, and password.  The rest of the 
            //- information could be left blank.
            MemberBioForm theMemberForm = new MemberBioForm();
                theMemberForm.setMemberId(theMember.getMemberId());
                //BeanUtils.copyProperties(theMemberForm, theMember);

            request.setAttribute("memberAddForm", theMemberForm);            
            return mapping.findForward("Add User");
            

        } else {
            if (menuForm.getAction().equalsIgnoreCase(Util.resources.getString("label.logout"))) {
                return mapping.findForward("welcome");
            }
        }
        try {
            saveMessages(request, messages);
            return mapping.findForward(menuForm.getAction());
        } catch (Exception e) {
            return mapping.findForward("future");

        }
    }
}
