package actions;

import beans.Member;
import business.MemberBO;
import forms.MemberBioForm;
import forms.UserForm;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

/**
 * This class will process the authentication from the login information
 * provided.
 *
 * @author bjmaclean
 * @since 20131129
 */
public class MemberBioUpdateAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        ActionMessages messages = new ActionMessages();

        /*
         Get the loginForm from the request.  Note that the code knows from the struts-config
         that loginForm is the form associated with the authenticate action
         */
        MemberBioForm memberBioForm = (MemberBioForm) request.getAttribute("memberBioForm");
        System.out.println("firstName entered=" + memberBioForm.getFirstName());

        //*************************************************************************
        // Pass the information to the business layer to update the information.
        //*************************************************************************
        Member theMember = new Member();
        BeanUtils.copyProperties(theMember, memberBioForm);
        ActionForward forwardTo = null;
        try {
            MemberBO.updateMember(theMember);
            messages.add("message1", (new ActionMessage("label.member.bio.update.success")));
            request.getSession().setAttribute("member", theMember);
            forwardTo = mapping.findForward("main");
        } catch (Exception e) {
            messages.add("error", (new ActionMessage("label.member.bio.update.fail")));
            request.setAttribute("memberBioForm", memberBioForm);
            forwardTo = mapping.findForward("memberBio");
        }

        saveMessages(request, messages);

        //*************************************************************************
        // Provide a message and return to the main tile.
        //*************************************************************************
        return forwardTo;
    }
}
