package actions;

import beans.ChangePassword;
import beans.Member;
import business.MemberBO;
import exceptions.PasswordException;
import forms.ChangePasswordForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

/**
 *
 * @author bjmaclean
 * @since 20131129
 */
public class ChangePasswordAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        ActionMessages messages = new ActionMessages();
        ActionForward forwardTo = null;
        forwardTo = mapping.findForward("main");
        saveMessages(request, messages);

        //*************************************************************************
        // Provide a message and return to the main tile.
        //*************************************************************************
        ChangePasswordForm changePasswordForm = (ChangePasswordForm) request.getAttribute("changePasswordForm");

        //*************************************************************************
        // Pass the information to the business layer to update the information.
        //*************************************************************************
        ChangePassword changePassword = new ChangePassword();
        BeanUtils.copyProperties(changePassword, changePasswordForm);
        //get the member to compare with their new passsword
        Member theMember = (Member) request.getSession().getAttribute("member");

        try {
            MemberBO.changePassword(theMember, changePassword);
            theMember.setPassword(changePassword.getNewPassword());
            request.getSession().setAttribute("member", theMember);
            messages.add("message1", (new ActionMessage("label.password.changed")));
        } catch (PasswordException pwException) {
            ActionMessage am = new ActionMessage(pwException.getMessage());
            if (pwException.getMessage().equalsIgnoreCase("Existing password does not match")) {
                messages.add("error", (new ActionMessage("label.exising.password.error")));
            } else if (pwException.getMessage().equalsIgnoreCase("New and confirm passwords do not match")) {
                messages.add("error", (new ActionMessage("label.confirm.password.error")));
            } else {
                messages.add("error", (new ActionMessage("label.error")));
            }
            
            forwardTo = mapping.findForward("ChangePassword");
        }
        saveMessages(request, messages);
        return forwardTo;
    }
}
