package actions;

import beans.Member;
import business.MemberBO;
import forms.MemberBioForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import util.Mail;

/**
 *
 * @author bjmaclean
 * @since 20131129
 */
public class MemberAddAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        ActionMessages messages = new ActionMessages();
        ActionForward forwardTo = null;
        forwardTo = mapping.findForward("main");
        saveMessages(request, messages);

        //*************************************************************************
        // Provide a message and return to the main tile.
        //*************************************************************************
        
        MemberBioForm memberBioForm = (MemberBioForm) request.getAttribute("memberAddForm");
        System.out.println("firstName entered=" + memberBioForm.getFirstName());

        //*************************************************************************
        // Pass the information to the business layer to update the information.
        //*************************************************************************
        Member theMember = new Member();
        BeanUtils.copyProperties(theMember, memberBioForm);
        MemberBO.addMember(theMember);
                        
        return forwardTo;
    }
}
