package actions;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import util.Mail;


/**
 * This class will process the authentication from the login information
 * provided.
 *
 * @author bjmaclean
 * @since 20131129
 */
public class InitializeAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {


        //Mail testMail = new Mail();
        //testMail.sendMail();
        return mapping.findForward("login");
    }
}
