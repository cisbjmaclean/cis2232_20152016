package actions;

import business.MemberEducationBO;
import business.NotificationBO;
import forms.MemberEducationForm;
import forms.NotificationForm;
import forms.UserForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

/**
 * This class will process the authentication from the login information
 * provided.
 *
 * @author bjmaclean
 * @since 20131129
 */
public class NotificationAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        ActionMessages messages = new ActionMessages();
        saveMessages(request, messages);

        //First check if the user is logged in
        UserForm loggedIn = (UserForm) request.getSession().getAttribute("user");
        if (loggedIn == null || !loggedIn.isAuthenticated()) {
            messages.add("error", (new ActionMessage("label.session.expired")));
            saveMessages(request, messages);
            return mapping.findForward("login");
        }

        //***************************************************************************
        // Check to see if the user chose to delete (action=delete)
        //***************************************************************************
        
        NotificationForm formNotification = (NotificationForm) request.getAttribute("notificationForm");
        String deleteAction = (String) request.getParameter("delete");
        System.out.println("action entered=" + deleteAction);

        ActionForward forwardTo = null;
        if (!(deleteAction == null) && deleteAction.length() > 0){
            //have to delete the member education with sequencce  which is in test.
            NotificationBO.deleteNotification(Integer.parseInt(deleteAction));
            
            //reload notifications
            request.getSession().setAttribute("Notifications", NotificationBO.getNotifications());

            messages.add("message1", (new ActionMessage("label.notification.deleted")));
            saveMessages(request, messages);

            forwardTo = mapping.findForward("Notification");
        } else {
            forwardTo = mapping.findForward("NotificationAdd");
            
        }
        
        //*************************************************************************
        // Provide a message and return to the main tile.
        //*************************************************************************
        return forwardTo;
    }
}
