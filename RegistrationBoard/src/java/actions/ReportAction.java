package actions;




import beans.Report;
import database.ReportsDAO;
import forms.ReportsForm;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 *
 * @author Roger Myers
 * @since 2014/06/24
 */
public class ReportAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        //Create a new reportsFrom from the request.
        ReportsForm reportsForm = (ReportsForm) request.getAttribute("reportForm");
        //Create a local array list from the DAO results.
        ArrayList<Report> reports = ReportsDAO.getReport(request, reportsForm.getMemberId());
        double y = 0;
        //Calculating the value given the weight and hours.
        for (int x = 0; x < reports.size(); ++x) {
            reports.get(x).setValue();
            y = y + reports.get(x).getValue();
            reportsForm.setTotal(y);
        }
        //Setting attributes back into the request.
        request.setAttribute("report", reports);
        //Going to the results of the report page.
        return mapping.findForward("Report Results");
    }

}
