package actions;

import beans.Member;
import beans.MemberEmployer;
import business.MemberEmploymentBO;
import forms.MemberEmployerForm;
import forms.MemberEmploymentForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import util.Util;

/**
 * This class will process the adding of employer details provided.
 *
 * @author bjmaclean
 * @since 20140615
 */
public class MemberEmployerAddAction extends Action {

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        ActionMessages messages = new ActionMessages();

        Member theMember = (Member) request.getSession().getAttribute("member");

//        messages.add("message1", (new ActionMessage("label.member.education.added")));
//        saveMessages(request, messages);
        MemberEmployerForm memberEmployerForm = (MemberEmployerForm) request.getAttribute("memberEmployerForm");
        ActionForward forwardTo = null;
        
        MemberEmployer me = new MemberEmployer();
        me.setUserId(theMember.getUserId());
        me.setMemberId(theMember.getMemberId());
        BeanUtils.copyProperties(me, memberEmployerForm);
        MemberEmploymentBO.insertMemberEmployer(me);
        
        forwardTo = mapping.findForward(util.Util.resources.getString("label.member.employment"));
        return forwardTo;
    }
}
