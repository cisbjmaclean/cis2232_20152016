package forms;

import database.CodeValueDAO;
import org.apache.struts.validator.ValidatorForm;

/**
 *
 * @author bjmaclean
 */
public class MemberBioForm extends ValidatorForm {
    private int memberId;
    private String userId = "";
    private String newUserId = "";
    private String accessString = "";
    private String firstName = "";
    private String middleName = "";
    private String lastName = "";
    private String previousSurnames = "";
    private int salutationCode;
    private boolean emailToMembers = false;
    private boolean emailToBusiness = false;
    private boolean emailToGovernment = false;
    private boolean emailToPEIDAExecutive = false;
    private String addressLine1 = "";
    private String addressLine2 = "";
    private String municipality = "";
    private int provinceCode;
    private String postalCode = "";
    private String homePhone = "";
    private String workPhone = "";
    private String workPhoneExtension = "";
    private String fax = "";
    private String emailAddress = "";
    private String website = "";
    private String dateOfBirth = "";
    private int genderCode;
    private boolean isBilingual;
    private String bilingualOther;
    private boolean isCanadianCitizen;
    private boolean isLandedImmigrant;
    private int countryOfOriginCode;
    private boolean isImmigrantAuthorized;
    private String immigrantAuthorizedExpiryDate;
    private String password;
    private int userType;

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    
    
    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getNewUserId() {
        return newUserId;
    }

    public void setNewUserId(String newUserId) {
        this.newUserId = newUserId;
    }

    
    public int getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(int genderCode) {
        this.genderCode = genderCode;
    }


    public String getBilingualOther() {
        return bilingualOther;
    }

    public void setBilingualOther(String bilingualOther) {
        this.bilingualOther = bilingualOther;
    }
    public int getCountryOfOriginCode() {
        return countryOfOriginCode;
    }

    public void setCountryOfOriginCode(int countryOfOriginCode) {
        this.countryOfOriginCode = countryOfOriginCode;
    }


    public String getImmigrantAuthorizedExpiryDate() {
        return immigrantAuthorizedExpiryDate;
    }

    public void setImmigrantAuthorizedExpiryDate(String immigrantAuthorizedExpiryDate) {
        this.immigrantAuthorizedExpiryDate = immigrantAuthorizedExpiryDate;
    }

    public boolean isIsBilingual() {
        return isBilingual;
    }

    public void setIsBilingual(boolean isBilingual) {
        this.isBilingual = isBilingual;
    }

    public boolean isIsCanadianCitizen() {
        return isCanadianCitizen;
    }

    public void setIsCanadianCitizen(boolean isCanadianCitizen) {
        this.isCanadianCitizen = isCanadianCitizen;
    }

    public boolean isIsLandedImmigrant() {
        return isLandedImmigrant;
    }

    public void setIsLandedImmigrant(boolean isLandedImmigrant) {
        this.isLandedImmigrant = isLandedImmigrant;
    }

    public boolean isIsImmigrantAuthorized() {
        return isImmigrantAuthorized;
    }

    public void setIsImmigrantAuthorized(boolean isImmigrantAuthorized) {
        this.isImmigrantAuthorized = isImmigrantAuthorized;
    }

    
    
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getWorkPhoneExtension() {
        return workPhoneExtension;
    }

    public void setWorkPhoneExtension(String workPhoneExtension) {
        this.workPhoneExtension = workPhoneExtension;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    
    

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    
    public int getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(int provinceCode) {
        this.provinceCode = provinceCode;
    }

    
    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    
    public boolean isEmailToMembers() {
        return emailToMembers;
    }

    public void setEmailToMembers(boolean emailToMembers) {
        this.emailToMembers = emailToMembers;
    }

    public boolean isEmailToBusiness() {
        return emailToBusiness;
    }

    public void setEmailToBusiness(boolean emailToBusiness) {
        this.emailToBusiness = emailToBusiness;
    }

    public boolean isEmailToGovernment() {
        return emailToGovernment;
    }

    public void setEmailToGovernment(boolean emailToGovernment) {
        this.emailToGovernment = emailToGovernment;
    }

    public boolean isEmailToPEIDAExecutive() {
        return emailToPEIDAExecutive;
    }

    public void setEmailToPEIDAExecutive(boolean emailToPEIDAExecutive) {
        this.emailToPEIDAExecutive = emailToPEIDAExecutive;
    }
    
    
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessString() {
        return accessString;
    }

    public void setAccessString(String accessString) {
        this.accessString = accessString;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPreviousSurnames() {
        return previousSurnames;
    }

    public void setPreviousSurnames(String previousSurnames) {
        this.previousSurnames = previousSurnames;
    }

    public int getSalutationCode() {
        return salutationCode;
    }

    public void setSalutationCode(int salutationCode) {
        this.salutationCode = salutationCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    


    
}
