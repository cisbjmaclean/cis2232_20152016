package forms;

import org.apache.struts.validator.ValidatorForm;

/**
 *
 * @author bjmaclean
 */
public class UserForm extends ValidatorForm {

    private String userId = "";
    private String password = "";
    private int userType;
    private boolean authenticated;
    private int memberId=0;

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }


    
}
