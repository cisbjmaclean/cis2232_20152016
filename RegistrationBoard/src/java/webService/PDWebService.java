package webService;

import database.ReportsDAO;
import beans.PDCount;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Roger Myers
 * @since 2014/06/24
 * Web service that returns the amount of people that attend a particular type
 * of PD event.
 */
@WebService(serviceName = "PDWebService")
public class PDWebService {

    /**
     * Connects to the database and returns a count.
     */
    @WebMethod(operationName = "get")
    public String get(String txt) {
        //Instantiate a new PDCount;
        PDCount pDCount = new PDCount();
        //Connect to the database and set the count field
        pDCount.setpDCount(ReportsDAO.getCount(Integer.parseInt(txt)));
        //Convert to xml format.
        return pDCount.toStringXML();
    }
}
