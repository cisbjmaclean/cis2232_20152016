/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author BJ
 */
public class Security {

    private static int memberId = 0;

    public static int getMemberId() {
        return memberId;
    }
    
    public static int authenticateUser(String userId, String password) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        boolean valid = false;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT member_id FROM member_access WHERE user_id = ? and password = ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1, userId);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            memberId = -1;
            if (rs.next()) {
                
                int memberFound = rs.getInt(1);
                memberId = memberFound;
                if (Util.debugOn) System.out.println("Member id found = "+memberFound);
                if (memberFound >= 1) valid = true;
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return memberId;
    }
}
