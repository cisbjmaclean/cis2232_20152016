<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html lang="true">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><bean:message key="welcome.title"/></title>
        <html:base/>
    </head>

    <body>
        <h2><bean:message key="label.notification"/></h2>
        <div>
            <html:form action="NotificationAdd">

                <table>                
                    <tr ><td>
                            <logic:messagesPresent message="true">
                                <html:messages id="msg2" message="true" property="message1"><div class="infoMessageCheck"><bean:write name="msg2"/></div><br/></html:messages>				  		
                                <html:messages id="msg2" message="true" property="warn"><div class="warnExclaim"><bean:write name="msg2"/></div><br/></html:messages>
                                <html:messages id="msg2" message="true" property="error"><div class="errorX"><bean:write name="msg2"/></div><br/></html:messages>				  		
                            </logic:messagesPresent>
                            <%-- the html:errors is populated if the validator is used. --%>    
                            <div style="color:red">
                                <html:errors />
                            </div>
                        </td></tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="notificationType">
                                <strong><bean:message key="label.notification.type" />:&nbsp;&nbsp;</strong></label>
                                <logic:iterate name="notification_types" id="notification_type" scope ="session">
                                    <html:radio property="notificationType" value="${notification_type.codeValueSequence}" >
                                        <bean:write name="notification_type" property="description"/>
                                    </html:radio>
                                </logic:iterate>
                        </td>
                    </tr>

                     <tr>
                    <td><label class="alignCenter"  for="notificationDetail">
                            <strong><bean:message key="label.notification.detail" /></strong></label><br />
                            <html:textarea  style="max-height:300px; max-width:500px; width: 300px; height: 150px;" property="notificationDetail"></html:textarea>
                        </td>
                    </tr>
                    
                    <tr/>
                    <tr align="center">
                        <td colspan="2">
                            <input type="submit" value=<bean:message key="label.submit"/>
                        </td>
                    </tr>
                </table>
            </html:form>
        </div>


    </body>

</html:html>
