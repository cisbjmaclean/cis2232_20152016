<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html lang="true">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><bean:message key="welcome.title"/></title>
        <html:base/>
        
       <script type="text/javascript">
function showDesignation(test) {
    if (test == 7) {
            document.getElementById('the_designation').style.display='none';
        //alert(test);
    } else {
        document.getElementById('the_designation').style.display='block';
    }
//    alert('test2');
}
</script> 
        
        
    </head>

    <body>
        <h2><bean:message key="label.member.education"/></h2>
        <div>
            <html:form action="MemberEducationAdd">

                <table>                
            <tr ><td>
                    <logic:messagesPresent message="true">
                        <html:messages id="msg2" message="true" property="message1"><div class="infoMessageCheck"><bean:write name="msg2"/></div><br/></html:messages>				  		
                        <html:messages id="msg2" message="true" property="warn"><div class="warnExclaim"><bean:write name="msg2"/></div><br/></html:messages>
                        <html:messages id="msg2" message="true" property="error"><div class="errorX"><bean:write name="msg2"/></div><br/></html:messages>				  		
                    </logic:messagesPresent>
                    <%-- the html:errors is populated if the validator is used. --%>    
                    <div style="color:red">
                        <html:errors />
                    </div>
                </td></tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="programCode">
                                <strong><bean:message key="label.program" />:&nbsp;&nbsp;</strong></label>
                                <logic:iterate name="programs" id="program" scope ="session">
                                    <html:radio property="programCode" value="${program.codeValueSequence}" onclick="showDesignation(this.value);">
                                        <bean:write name="program" property="description"/>
                                    </html:radio>
                            </logic:iterate>
                        </td>
                    </tr>

                    <tr>
                        <td id="the_designation">
                            <label class="alignCenter"  for="designation">
                                <strong><bean:message key="label.designation" /></strong></label>
                                <html:text  property="designation" size="30" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="year">
                                <strong><bean:message key="label.year.obtained" /></strong></label>
                                <html:text property="year" size="4" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <label class="alignCenter"  for="provinceCode">
                                <strong><bean:message key="label.province" /></strong></label>
                                <html:select property="provinceCode">  
                                    <html:optionsCollection  name="provinces" value="codeValueSequence" label="description" />  
                                </html:select>                         
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <label class="alignCenter"  for="institution">
                                <strong><bean:message key="label.institution.name" /></strong></label>
                                <html:text property="institution" size="40" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="2">
                            <html:checkbox property="coreIndicator" >
                                <bean:message key="label.core.indicator"/>
                            </html:checkbox>
                        </td>
                    </tr>
                    
                    <tr/>
                    <tr align="center">
                        <td colspan="2">
                            <input type="submit" value=<bean:message key="label.submit"/>
                        </td>
                    </tr>
                </table>
            </html:form>
        </div>


    </body>

</html:html>
