<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html lang="true">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><bean:message key="welcome.title"/></title>
        <html:base/>
        <script type="text/javascript">
            function deleteEducation(test, educationDescription) {
                var r = confirm("<bean:message key="label.confirm.delete.education"/>" + educationDescription + "?");
                if (r == true) {
                    var s = document.getElementById('action_id');
                    s.value = test;

                    document.forms[1].action = "/RegistrationBoard/MemberEducation.do?delete=" + test;
                    document.forms[1].submit();
                }

            }
        </script> 

    </head>

    <body>
        <h2><bean:message key="label.member.education"/></h2>
        <div>
            <html:form  action="MemberEducation">
                <input type="hidden" id="action_id" property="action"/>
                <table>                
                    <tr ><td>
                            <logic:messagesPresent message="true">
                                <html:messages id="msg2" message="true" property="message1"><div class="infoMessageCheck" ><bean:write name="msg2"/></div><br/></html:messages>				  		
                                <html:messages id="msg2" message="true" property="warn"><div class="warnExclaim"><bean:write name="msg2"/></div><br/></html:messages>
                                <html:messages id="msg2" message="true" property="error"><div class="errorX"><bean:write name="msg2"/></div><br/></html:messages>				  		
                            </logic:messagesPresent>
                            <%-- the html:errors is populated if the validator is used. --%>    
                            <div style="color:red">
                                <html:errors />
                            </div>
                        </td></tr>
                    <tr>
                        <td>
                            <hr/>
                        </td>
                    </tr>
                    <logic:iterate name="Education" id="ThisEducation" scope ="session">

                        <tr>
                            <td>
                                <label class="alignCenter" for="programDescription">
                                    <strong><bean:message key="label.program" />:&nbsp;&nbsp;</strong></label>
                                <bean:write name="ThisEducation" property="programDescription"/><logic:equal name="ThisEducation" property="coreIndicator" value="true">&nbsp;(<bean:message key="label.core" />)</logic:equal>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="alignCenter" for="designation">
                                        <strong><bean:message key="label.designation" />:&nbsp;&nbsp;</strong></label>
                                    <bean:write name="ThisEducation" property="designation"/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="alignCenter" for="year">
                                    <strong><bean:message key="label.year.obtained" />:&nbsp;&nbsp;</strong></label>
                                    <bean:write name="ThisEducation" property="year"/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="alignCenter" for="provinceDescription">
                                    <strong><bean:message key="label.province" />:&nbsp;&nbsp;</strong></label>
                                    <bean:write name="ThisEducation" property="provinceDescription"/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="alignCenter" for="institution">
                                    <strong><bean:message key="label.institution.name" />:&nbsp;&nbsp;</strong></label>
                                    <bean:write name="ThisEducation" property="institution"/>
                            </td>
                        </tr>
                        <tr align="center">
                            <td colspan="2">
                                <input type="button" 
                                       onclick="deleteEducation(<bean:write name="ThisEducation" property="memberEducationSequence"/>, '<bean:write name="ThisEducation" property="programDescription"/>')" value='<bean:message key="label.delete"/>'>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <hr/>
                            </td>
                        </tr>

                    </logic:iterate>


                    <tr/>
                    <tr align="center">
                        <td colspan="2">
                            <input type="submit" value=<bean:message key="label.add.education"/>/>
                        </td>
                    </tr>
                </table>
            </html:form>
        </div>


    </body>

</html:html>
