<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html lang="true">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="styles/styles.css">
        <title><bean:message key="welcome.title"/></title>
    </head>

    <body>
    <center>
        <h2><bean:message key="label.login"/></h2>
    </center>
    <div>
        <html:form action="Login">
            <center>
                <table>
                    <tr><td>
                            <logic:messagesPresent message="true">
                                <html:messages id="msg2" message="true" property="message1"><div class="infoMessageCheck" style="color: green"><bean:write name="msg2"/></div><br/></html:messages>				  		
                                <html:messages id="msg2" message="true" property="warn"><div class="warnExclaim"  style="color: yellow"><bean:write name="msg2"/></div><br/></html:messages>
                                <html:messages id="msg2" message="true" property="error"><div class="errorX"  style="color: red"><bean:write name="msg2"/></div><br/></html:messages>				  		
                            </logic:messagesPresent>
                            <%-- the html:errors is populated if the validator is used. --%>    
                            <div style="color:red">
                                <html:errors />
                            </div>
                        </td></tr>



                    <tr>
                        <td>
                            <label class="alignCenter" for="userId">
                                <strong><bean:message key="label.username" /></strong></label>
                            <input type="text" name="userId" id="userId" size="20" value="bjmaclean" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="password">
                                <strong><bean:message key="label.password" /></strong></label>
                            <input type="password" id="password" name="password" size="20" value="bjmacleanpw" />
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <input type="submit" value="submit"/>
                            <input type="reset" value="reset"/>
                        </td>
                    </tr>
                </table>
            </center>
        </html:form>
    </div>


</body>

</html:html>
