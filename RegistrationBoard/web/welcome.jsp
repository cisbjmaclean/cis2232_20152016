<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html lang="true">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><bean:message key="welcome.title"/></title>
        <html:base/>
        <link rel="stylesheet" type="text/css" href="styles/styles.css">
    </head>
    <html:form action="/Initialize">
        <body>
        <center>
            <h2><bean:message key="welcome.message"/></h2>
            <html:submit value="Enter"/>
        </center>
    </body>
</html:form>
</html:html>
