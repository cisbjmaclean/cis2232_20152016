<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html lang="true">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><bean:message key="welcome.title"/></title>
        <html:base/>

         <script type="text/javascript">
            function deleteNotification(test, description) {
                var r = confirm("<bean:message key="label.confirm.delete.notification"/>" + description + "?");
                if (r == true) {
                    var s = document.getElementById('action_id');
                    s.value = test;

                    document.forms[1].action = "/RegistrationBoard/Notification.do?delete=" + test;
                    document.forms[1].submit();
                }

            }
        </script> 

    </head>

    <body>
        <h2><bean:message key="label.notification"/></h2>
        <div>
            <html:form  action="Notification">
                <input type="hidden" id="action_id" property="action"/>
                <table>                
                    <tr ><td>
                            <logic:messagesPresent message="true">
                                <html:messages id="msg2" message="true" property="message1"><div class="infoMessageCheck" ><bean:write name="msg2"/></div><br/></html:messages>				  		
                                <html:messages id="msg2" message="true" property="warn"><div class="warnExclaim"><bean:write name="msg2"/></div><br/></html:messages>
                                <html:messages id="msg2" message="true" property="error"><div class="errorX"><bean:write name="msg2"/></div><br/></html:messages>				  		
                            </logic:messagesPresent>
                            <%-- the html:errors is populated if the validator is used. --%>    
                            <div style="color:red">
                                <html:errors />
                            </div>
                        </td></tr>
                    
                       <logic:iterate name="Notifications" id="ThisNotification" scope ="session">

                        <tr>
                            <td>
                                <logic:equal name="ThisNotification" property="notificationType" value="2"><strong></logic:equal>
                                <bean:write name="ThisNotification" property="notificationDetail"/>
                                <logic:equal name="ThisNotification" property="notificationType" value="2"></strong></logic:equal>
                                </td>
                            </tr>
                            
                                                    <tr align="center">
                            <td>
                                <input type="button" 
                                       onclick="deleteNotification(<bean:write name="ThisNotification" property="notificationId"/>, '<bean:write name="ThisNotification" property="notificationDetail"/>')" value='<bean:message key="label.delete"/>'>
                            </td>
                        </tr>

                        </logic:iterate>
                    <tr>
                        <td>
                            <hr/>
                        </td>
                    </tr>

                    <tr/>
                    <tr align="center">
                        <td colspan="2">
                            <input type="submit" value=<bean:message key="label.add.notification"/>/>
                        </td>
                    </tr>
                </table>
            </html:form>
        </div>


    </body>

</html:html>
