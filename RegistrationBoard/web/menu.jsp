<%-- 
    Document   : main
    Created on : Nov 13, 2013, 2:51:24 PM
    Author     : bjmaclean
--%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
    <head></head>
<html:form action="/Menu">
<table>
<tr>
    <td>
        <h2><bean:message key="label.menu" /></h2>
  </td>
</tr>

<%-- This will filter the member button for the member user types 
<logic:notEqual name="user" property="userType" value="1" scope="session">
<tr>
  <td width="120" valign="top">
      <font size="-1"> <input type="submit" name="action" value="Future"/></font>
  </td>
</tr>
</logic:notEqual>
--%>

<tr>
  <td>
      <h3><bean:message key="label.member.details"/></h3>
  </td>
</tr>
<tr>
  <td>
      <input type="submit" name="action" value="<bean:message key='label.basic'/>" />
  </td>
</tr>
<tr>
  <td>
      <input type="submit" name="action" value="<bean:message key='label.education'/>"/>
  </td>
</tr>
<tr>
  <td>
    <input type="submit" name="action" value="<bean:message key='label.employment'/>"/>
  </td>
</tr>
<tr>
  <td>
      <input type="submit" name="action" value="<bean:message key='label.professional.development'/>"/>
  </td>
</tr>
<tr>
  <td>
      <h3><bean:message key="label.tools"/></h3>
  </td>
</tr>
<tr>
  <td>
    <input type="submit" name="action" value="<bean:message key='label.reports'/>"/>
  </td>
</tr>
<tr>
  <td>
    <input type="submit" name="action" value="<bean:message key='label.change.password'/>"/>
  </td>
</tr>
<logic:equal name="user_type" scope="session" value="1">
<tr>
  <td>
    <input type="submit" name="action"  value="<bean:message key='label.notification'/>"/>
  </td>
</tr>
</logic:equal>
<tr>
  <td>
    <input type="submit" name="action" value="<bean:message key='label.logout'/>"/>
  </td>
</tr>

<logic:equal name="user_type" scope="session" value="1">
<tr>
  <td>
      <h3><bean:message key="label.administration"/></h3>
  </td>
</tr>
<tr>
  <td>
    <input type="submit" name="action" value="<bean:message key='label.add.user'/>"/>
  </td>
</tr>
</logic:equal>

</table>
</html:form>
</html>