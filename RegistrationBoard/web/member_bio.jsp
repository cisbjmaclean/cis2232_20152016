<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html lang="true">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><bean:message key="welcome.title"/></title>
        <html:base/>
    </head>

    <body>
        <h2><bean:message key="label.member.bio"/></h2>
        <div>
            <html:form action="MemberBioUpdate">

                <table>                
                    <tr ><td>
                            <logic:messagesPresent message="true">
                                <html:messages id="msg2" message="true" property="message1"><div class="infoMessageCheck" style="color: green"><bean:write name="msg2"/></div><br/></html:messages>				  		
                                <html:messages id="msg2" message="true" property="warn"><div class="warnExclaim"  style="color: yellow"><bean:write name="msg2"/></div><br/></html:messages>
                                <html:messages id="msg2" message="true" property="error"><div class="errorX"  style="color: red"><bean:write name="msg2"/></div><br/></html:messages>				  		
                            </logic:messagesPresent>
                            <%-- the html:errors is populated if the validator is used. --%>    
                            <div style="color:red">
                                <html:errors />
                            </div>
                        </td></tr>
                    <tr><td><hr/></td></tr>

                    <tr>
                        <td><html:hidden property="memberId"/>
                            <label class="alignCenter" for="firstName">
                                <strong><bean:message key="label.first.name" /></strong></label>
                                <html:text property="firstName" size="20" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter" for="middleName">
                                <strong><bean:message key="label.middle.name" /></strong></label>
                                <html:text property="middleName" size="20" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="lastName">
                                <strong><bean:message key="label.last.name" /></strong></label>
                                <html:text property="lastName" size="20" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="previousSurnames">
                                <strong><bean:message key="label.previous.surnames" /></strong></label>
                                <html:text property="previousSurnames" size="50" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="salutation">
                                <strong><bean:message key="label.salutation" />:&nbsp;&nbsp;</strong></label>
                                <logic:iterate name="salutations" id="salutation" scope ="session">
                                    <html:radio property="salutationCode" value="${salutation.codeValueSequence}" >
                                        <bean:write name="salutation" property="description"/>
                                    </html:radio>

                            </logic:iterate>
                        </td>
                    </tr>
                    <tr/>
                    <tr>
                        <td colspan="2">
                            <label class="alignCenter"  for="permission1">
                                <strong><bean:message key="label.email.permission" /></strong></label>
                        </td>
                    </tr>
                    <tr/>
                    <tr>
                        <td colspan="2">
                            <html:checkbox property="emailToMembers" >
                                <bean:message key="label.email.members"/>
                            </html:checkbox>
                            <html:checkbox property="emailToBusiness"  >
                                <bean:message key="label.email.business"/>
                            </html:checkbox>
                            <html:checkbox property="emailToGovernment"  >
                                <bean:message key="label.email.government"/>
                            </html:checkbox>
                            <html:checkbox property="emailToPEIDAExecutive" >
                                <bean:message key="label.email.PEIDA.executive"/>
                            </html:checkbox>
                        </td>
                    </tr>
                    <tr/>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="addressLine1">
                                <strong><bean:message key="label.address.line.1" /></strong></label>
                                <html:text property="addressLine1" size="50" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="addressLine2">
                                <strong><bean:message key="label.address.line.2" /></strong></label>
                                <html:text property="addressLine2" size="50" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="municipality">
                                <strong><bean:message key="label.municipality" /></strong></label>
                                <html:text property="municipality" size="50" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="provinceCode">
                                <strong><bean:message key="label.province" /></strong></label>
                                <html:select property="provinceCode">  
                                    <html:optionsCollection  name="provinces" value="codeValueSequence" label="description" />  
                                </html:select>                         
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="postalCode">
                                <strong><bean:message key="label.postal.code" /></strong></label>
                                <html:text property="postalCode" size="6" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="homePhone">
                                <strong><bean:message key="label.home.phone" /></strong></label>
                                <html:text property="homePhone" size="10" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="workPhone">
                                <strong><bean:message key="label.work.phone" /></strong></label>
                                <html:text property="workPhone" size="10" />
                            <label class="alignCenter"  for="workPhoneExtension">
                                <strong><bean:message key="label.work.phone.extension" /></strong></label>
                                <html:text property="workPhoneExtension" size="3" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="fax">
                                <strong><bean:message key="label.fax" /></strong></label>
                                <html:text property="fax" size="10" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="emailAddress">
                                <strong><bean:message key="label.email.address" /></strong></label>
                                <html:text property="emailAddress" size="25" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="website">
                                <strong><bean:message key="label.website" /></strong></label>
                                <html:text property="website" size="25" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="dateOfBirth">
                                <strong><bean:message key="label.date.of.birth" /></strong></label>
                                <html:text property="dateOfBirth" size="10" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="dateOfBirth">
                                <strong><bean:message key="label.date.of.birth" /></strong></label>
                                <html:text property="dateOfBirth" size="10" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="genderCode">
                                <strong><bean:message key="label.gender" /></strong></label>
                                <html:select property="genderCode">  
                                    <html:optionsCollection  name="genders" value="codeValueSequence" label="description" />  
                                </html:select>                         
                        </td>
                    </tr>

                    <tr>
                        <td colspan="1">
                            <html:checkbox property="isBilingual" >
                                <bean:message key="label.is.bilingual"/>
                            </html:checkbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="bilingualOther">
                                <strong><bean:message key="label.bilingual.other" /></strong></label>
                                <html:text property="bilingualOther" size="50" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="1">
                            <html:checkbox property="isCanadianCitizen" >
                                <bean:message key="label.is.canadian.citizen"/>
                            </html:checkbox>&nbsp;&nbsp;
                            <html:checkbox property="isLandedImmigrant" >
                                <bean:message key="label.is.landed.immigrant"/>
                            </html:checkbox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="countryOfOriginCode">
                                <strong><bean:message key="label.country.of.origin" /></strong></label>
                                <html:select property="countryOfOriginCode">  
                                    <html:optionsCollection  name="countries" value="codeValueSequence" label="description" />  
                                </html:select>                         
                        </td>
                    </tr>


                    <tr>
                        <td colspan="1">
                            <html:checkbox property="isImmigrantAuthorized" >
                                <bean:message key="label.immigrant.authorized"/>
                            </html:checkbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="immigrantAuthorizedExpiryDate">
                                <strong><bean:message key="label.immigrant.authorized.expiry" /></strong></label>
                                <html:text property="immigrantAuthorizedExpiryDate" size="50" />
                        </td>
                    </tr>

                    <tr/>
                    <tr align="center">
                        <td colspan="2">
                            <input type="submit" value=<bean:message key="label.submit"/>
                        </td>
                    </tr>
                </table>
            </html:form>
        </div>


    </body>

</html:html>
