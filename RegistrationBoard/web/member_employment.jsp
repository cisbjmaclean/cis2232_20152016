<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html lang="true">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><bean:message key="welcome.title"/></title>
        <html:base/>
        <script type="text/javascript">
            function deleteEmployer(test, description) {
                var r = confirm("<bean:message key="label.confirm.delete"/>" + description + "?");
                if (r == true) {
                    var s = document.getElementById('action_id');
                    s.value = test;

                    document.forms[1].action = "/RegistrationBoard/MemberEmployment.do?delete=" + test;
                    document.forms[1].submit();
                }

            }
        </script> 

    </head>

    <body>
        <h2><bean:message key="label.member.employment"/></h2>
        <div>
            <html:form  action="MemberEmployment">
                
                <table>                
                    <tr ><td>
                            <logic:messagesPresent message="true">
                                <html:messages id="msg2" message="true" property="message1"><div class="infoMessageCheck" ><bean:write name="msg2"/></div><br/></html:messages>				  		
                                <html:messages id="msg2" message="true" property="warn"><div class="warnExclaim"><bean:write name="msg2"/></div><br/></html:messages>
                                <html:messages id="msg2" message="true" property="error"><div class="errorX"><bean:write name="msg2"/></div><br/></html:messages>				  		
                            </logic:messagesPresent>
                            <%-- the html:errors is populated if the validator is used. --%>    
                            <div style="color:red">
                                <html:errors />
                            </div>
                        </td></tr>
                    <tr>
                        <td>
                            <hr/>
                        </td>
                    </tr>
                    <tr>
                        <td>                <input type="hidden" id="action_id" property="action"/>
                            <label class="alignCenter"  for="statusCode">
                                <strong><bean:message key="label.employment.status" />:&nbsp;&nbsp;</strong></label>
                        </td>
                    </tr>

                    <logic:iterate name="employment_statuses" id="status" scope ="session">
                        <tr>
                            <td>
                                <html:radio property="statusCode" value="${status.codeValueSequence}" >
                                    <bean:write name="status" property="description"/>
                                </html:radio>
                            </td>
                        </tr>

                    </logic:iterate>
                        <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="currencyCode">
                                <strong><bean:message key="label.currency" />:&nbsp;&nbsp;</strong></label>
                                <bean:message key="label.currency.description" />:&nbsp;&nbsp;</label>
                        </td>
                    </tr>

                    <logic:iterate name="currencies" id="theCurrency" scope ="session">
                        <tr>
                            <td>
                                <html:radio property="currencyCode" value="${theCurrency.codeValueSequence}" >
                                    <bean:write name="theCurrency" property="description"/>
                                </html:radio>
                            </td>
                        </tr>

                    </logic:iterate>

                        <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td colspan="2">
                            <label class="alignCenter">
                                <strong><bean:message key="label.practice.in.other.jurisdications" /></strong></label>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="2">
                            <html:checkbox property="practiceViaTelephoneIndicator" >
                                <bean:message key="label.via.telephone"/>
                            </html:checkbox>
                            <html:checkbox property="practiceViaInternetIndicator"  >
                                <bean:message key="label.via.internet"/>
                            </html:checkbox>
                            <html:checkbox property="practiceInPersonIndicator"  >
                                <bean:message key="label.in.person"/>
                            </html:checkbox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="practiceJurisdictions">
                                <strong><bean:message key="label.jurisdictions" /></strong></label>
                                <html:textarea property="practiceJurisdictions"  />
                        </td>
                    </tr>

                    <tr align="center">
                        <td colspan="2">
                            <input type="submit" name="action"  value=<bean:message key="label.save"/>>
                        </td>
                    </tr>

                    
                    <tr>
                        <td>
                            <hr/>
                        </td>
                    </tr>

                    
                    <tr>
                        <td>
                            <label class="alignCenter">
                                <strong><bean:message key="label.employers" /></strong></label>
                                
                        </td>
                    </tr>
                    
                    <logic:iterate name="Employers" id="ThisEmployer" scope ="session">

            <tr>
                        <td colspan="2">
                            <html:checkbox name="ThisEmployer" property="primaryIndicator"  disabled="true">
                                <bean:message key="label.primary.indicator"/>
                            </html:checkbox>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <label class="alignCenter" for="name">
                                <strong><bean:message key="label.name" /></strong></label>
                                <html:text name="ThisEmployer" property="name" size="40" disabled="true" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="addressLine1">
                                <strong><bean:message key="label.address.line.1" /></strong></label>
                                <html:text name="ThisEmployer" property="addressLine1" size="50" disabled="true" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="addressLine2">
                                <strong><bean:message key="label.address.line.2" /></strong></label>
                                <html:text name="ThisEmployer" property="addressLine2" size="50" disabled="true" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="municipality">
                                <strong><bean:message key="label.municipality" /></strong></label>
                                <html:text name="ThisEmployer" property="municipality" size="50"  disabled="true"/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="provinceCode">
                                <strong><bean:message key="label.province" /></strong></label>
                                <html:select name="ThisEmployer" property="provinceCode" disabled="true">  
                                    <html:optionsCollection  name="provinces" value="codeValueSequence" label="description" />  
                                </html:select>                         
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="postalCode">
                                <strong><bean:message key="label.postal.code"  /></strong></label>
                                <html:text name="ThisEmployer" property="postalCode" size="6"  disabled="true"/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="positionCode">
                                <strong><bean:message key="label.position" /></strong></label>
                                <html:select name="ThisEmployer" property="positionCode" disabled="true">  
                                    <html:optionsCollection  name="positions" value="codeValueSequence" label="description" />  
                                </html:select>                         
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="practiceAreaCode">
                                <strong><bean:message key="label.practice.area" /></strong></label>
                                <html:select name="ThisEmployer" property="practiceAreaCode" disabled="true">  
                                    <html:optionsCollection  name="practice_areas" value="codeValueSequence" label="description" />  
                                </html:select>                         
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="employmentStatusCode">
                                <strong><bean:message key="label.employment.status" /></strong></label>
                                <html:select name="ThisEmployer" property="employmentStatusCode" disabled="true">  
                                    <html:optionsCollection  name="employment_statuses" value="codeValueSequence" label="description" />  
                                </html:select>                         
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="employmentCategoryCode">
                                <strong><bean:message key="label.employment.category" /></strong></label>
                                <html:select name="ThisEmployer" property="employmentCategoryCode" disabled="true">  
                                    <html:optionsCollection  name="employment_categories" value="codeValueSequence" label="description" />  
                                </html:select>                         
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="fundingSourceCode">
                                <strong><bean:message key="label.funding.source" /></strong></label>
                                <html:select name="ThisEmployer" property="fundingSourceCode" disabled="true">  
                                    <html:optionsCollection  name="funding_sources" value="codeValueSequence" label="description" />  
                                </html:select>                         
                        </td>
                    </tr>

                        <tr align="center">
                            <td colspan="2">
                                <input type="button" 
                                       onclick="deleteEmployer(<bean:write name="ThisEmployer" property="sequence"/>, '<bean:write name="ThisEmployer" property="name"/>')" value='<bean:message key="label.delete"/>'>
                            </td>
                        </tr>

                    </logic:iterate>

                    

                    <tr align="center">
                        <td colspan="2">
                            <input type="submit" name="action" value="Add Employer">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <hr/>
                        </td>
                    </tr>

                </table>
            </html:form>
        </div>


    </body>

</html:html>
