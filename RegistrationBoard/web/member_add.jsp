<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html lang="true">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><bean:message key="welcome.title"/></title>
        <html:base/>
    </head>

    <body>
        <h2><bean:message key="label.add.user"/></h2>
        <div>
            <html:form action="MemberAdd">

                <table>                
                    <tr ><td>
                            <logic:messagesPresent message="true">
                                <html:messages id="msg2" message="true" property="message1"><div class="infoMessageCheck" style="color: green"><bean:write name="msg2"/></div><br/></html:messages>				  		
                                <html:messages id="msg2" message="true" property="warn"><div class="warnExclaim"  style="color: yellow"><bean:write name="msg2"/></div><br/></html:messages>
                                <html:messages id="msg2" message="true" property="error"><div class="errorX"  style="color: red"><bean:write name="msg2"/></div><br/></html:messages>				  		
                            </logic:messagesPresent>
                            <%-- the html:errors is populated if the validator is used. --%>    
                            <div style="color:red">
                                <html:errors />
                            </div>
                        </td></tr>
                    <tr><td><hr/></td></tr>

                    <tr>
                        <td>
                            <label class="alignCenter" for="newUserId">
                                <strong><bean:message key="label.username" /></strong></label>
                                <html:text property="newUserId" size="20" />
                        </td>
                    </tr>
                    <tr>
                        <td><html:hidden property="memberId"/>
                            <label class="alignCenter" for="firstName">
                                <strong><bean:message key="label.first.name" /></strong></label>
                                <html:text property="firstName" size="20" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter" for="middleName">
                                <strong><bean:message key="label.middle.name" /></strong></label>
                                <html:text property="middleName" size="20" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="lastName">
                                <strong><bean:message key="label.last.name" /></strong></label>
                                <html:text property="lastName" size="20" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="emailAddress">
                                <strong><bean:message key="label.email.address" /></strong></label>
                                <html:text property="emailAddress" size="30" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="alignCenter"  for="password">
                                <strong><bean:message key="label.temp.password" /></strong></label>
                                <html:text property="password" size="20" disabled="true" value="rboard"/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="alignCenter"  for="userType">
                                <strong><bean:message key="label.user.type" />:&nbsp;&nbsp;</strong></label>
                                <logic:iterate name="user_types" id="userType" scope ="session">
                                    <html:radio property="userType" value="${userType.codeValueSequence}" >
                                        <bean:write name="userType" property="description"/>
                                    </html:radio>

                            </logic:iterate>
                        </td>
                    </tr>

                    <tr/>
                    <tr align="center">
                        <td colspan="2">
                            <input type="submit" value=<bean:message key="label.submit"/>
                        </td>
                    </tr>
                </table>
            </html:form>
        </div>


    </body>

</html:html>
