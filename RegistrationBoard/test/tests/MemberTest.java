/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tests;


import beans.Member;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author bjmaclean
 */
public class MemberTest {
    
    public MemberTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        
    }

    @Test
    public void testMemberConstructor() {
    Member testMember = null;
        try {
            testMember = new Member(1);
        } catch (Exception ex) {
            Logger.getLogger(MemberTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals("Member name should be Bruce", "Bruce", testMember.getFirstName());
        
    }
    
    @Test
    public void testMemberConstructorInvalid() {
    Member testMember = null;
        try {
            testMember = new Member();
        } catch (Exception ex) {
            Logger.getLogger(MemberTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals("Member name should be BJ", "Bruce", testMember.getFirstName());
        
    }
    
    
}
