/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tests;

import beans.Member;
import business.MemberBO;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bjmaclean
 */
public class MemberBOTest {
    
    private Member testMember = null;
    
    public MemberBOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        try {
            testMember = new Member(1);
        } catch (Exception ex) {
            Logger.getLogger(MemberTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @After
    public void tearDown() {
        testMember.setFirstName("Bruce");
        try {
            MemberBO.updateMember(testMember);
        } catch (Exception ex) {
            Logger.getLogger(MemberBOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testUpdate() {
     
        assertEquals("Initial name should be BJ", "Bruce", testMember.getFirstName());         
        testMember.setFirstName("BruceTEST");
        try {
            MemberBO.updateMember(testMember);
        } catch (Exception ex) {
            Logger.getLogger(MemberBOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            testMember = new Member(1);
        } catch (Exception ex) {
            Logger.getLogger(MemberBOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals("Member name should be BruceTEST", "BruceTEST", testMember.getFirstName());         
     }
}
