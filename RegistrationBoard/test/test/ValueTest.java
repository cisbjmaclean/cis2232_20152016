package test;

import beans.Report;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Roger Myers
 * @since 2014/06/25
 * @Purpose This test will test whether the value is being set correctly.
 */
public class ValueTest {
    
    public ValueTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void valueTest(){
        Report rp = new Report();
        rp.setHour(4);
        rp.setWeight(0.5);
        rp.setValue();
        double expectedValue = 2.0;
        double result = rp.getValue();
        double delta = 1e-15;
        assertEquals(expectedValue, result, delta);
    
    }
    
    @Test
    public void weightTest(){
        Report rp = new Report();
        rp.setWeight(4);
        rp.setHour(0.0);
        rp.setValue();
        double expectedValue = 4.0;
        double result = rp.getValue();
        double delta = 1e-15;
        assertEquals(expectedValue, result, delta);
    
    }

}
