/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import beans.Event;
import database.ProfessionalDevelopmentDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import util.ConnectionUtils;
import util.DbUtils;

/**
 * This test will add a new event then delete it.
 * @author Roger Myers
 * @since 2014/06/24
 */
public class EventTest {

    public EventTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //Method that counts results in the event table.
    public int getCountEvent() {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        int count = 0;
        try {
            conn = ConnectionUtils.getConnection();
            sql = "SELECT COUNT(*) FROM event";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                count = rs.getInt(1);
            }

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        System.out.println("Count=" + count);
        return count;
    }

    //Method that deletes the newly inserted event.
    public void deleteTestDataEvent() {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();
            sql = "DELETE FROM event WHERE description = 'unittesting'";
            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

    }
    
    //Method that creates a new event and passed to the update database mthod.
    @Test
    public void testInsertEvent() {

        int initialCount = getCountEvent();

        Event event = new Event();
        event.setDate("10/07/1987");
        //setting the description to unittesting so I can identify and delete later.
        event.setDescription("unittesting");
        event.setHour(0.0);
        event.setMemberId(1);
        event.setPdCode(3);
        event.setPdDescription("unitTesting");

        try {
            //Passing the vent created above to the update method.
            ProfessionalDevelopmentDAO.updateEvent(event);
        } catch (Exception ex) {
            Logger.getLogger(TestEducation.class.getName()).log(Level.SEVERE, null, ex);
        }

        int postCount = getCountEvent();

        //Assert that one more row was found.
        assertEquals(initialCount + 1, postCount);
        deleteTestDataEvent();
    }
}
