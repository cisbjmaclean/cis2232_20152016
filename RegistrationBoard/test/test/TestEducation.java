/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import beans.MemberEducation;
import business.MemberEducationBO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import util.ConnectionUtils;
import util.DbUtils;

/**
 *
 * @author bjmaclean
 */
public class TestEducation {

    public TestEducation() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
        
        
    }

    public int getCoreEducationCount() {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        int count = 0;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "select count(*) from member_education where member_id = 1 and me_core_ind=1";

            ps = conn.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) count = rs.getInt(1);

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        System.out.println("Count=" + count);
        return count;
    }


        public void deleteTestData() {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        int count = 0;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "delete from member_education where created_user_id = 'unittesting'";

            ps = conn.prepareStatement(sql);

            ps.executeUpdate();
            

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

    }


    
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //

    @Test
    public void testInsert() {

        int initialCount = getCoreEducationCount();
        
        //First get the count of core education
    //run the insert method
        MemberEducation memberEducation = new MemberEducation();
        memberEducation.setCoreIndicator(true);
        memberEducation.setDesignation("test");
        memberEducation.setInstitution("UPEI");
        memberEducation.setActiveIndicator(true);
        memberEducation.setMemberId(1);
        memberEducation.setMemberEducationSequence(999);
        memberEducation.setProgramCode(1);
        memberEducation.setProgramDescription("test");
        memberEducation.setUserId("unittesting");

        try {
            MemberEducationBO.insertMemberEducation(memberEducation);

            //Asset the the count is now one more
        } catch (Exception ex) {
            Logger.getLogger(TestEducation.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        int postCount = getCoreEducationCount();
        
        //Assert that one more row was found.
        assertEquals(initialCount+1, postCount);
        
        deleteTestData();
        
        
        
    }

}
