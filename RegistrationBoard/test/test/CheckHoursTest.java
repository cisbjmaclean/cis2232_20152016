/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import beans.ProfessionalDevelopment;
import database.ProfessionalDevelopmentDAO;
import forms.EventForm;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rmyers6284
 */
public class CheckHoursTest {
    
    public CheckHoursTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void checkHoursTest(){
        System.out.println("checkHours");
        EventForm ev = new EventForm();
        //Setting pd code to a code that requires hours.
        ev.setPdCode(4);
        boolean expResult = true;
        //Loads the arraylist that the check hours method uses.
        ProfessionalDevelopmentDAO.getProfessionalDevelopment();
        ProfessionalDevelopment pd = new ProfessionalDevelopment();
        //Runnings the check hours passing the event with the pdCode of 4
        boolean result = pd.checkHours(ev);
        System.out.println(result);
        assertEquals(expResult, result);
        
    }
}
